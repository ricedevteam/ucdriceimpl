/*
 * Copyright 2008-2011 The Regents of the University of California
 *
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.opensource.org/licenses/ecl2.php
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.ucdavis.iet.whitepages;

public class WhitePagesPersonImpl implements WhitePagesPerson {

	private String mothraId;
	private String preferredFirstName;
	private String preferredLastName;
	private String preferredMiddleName;
	private String preferredSuffix;
	private boolean isNameReleased;
	
	public String getMothraId() {
		return mothraId;
	}

	public String getPreferredFirstName() {
		return preferredFirstName;
	}

	public String getPreferredLastName() {
		return preferredLastName;
	}

	public String getPreferredMiddleName() {
		return preferredMiddleName;
	}

	public String getPreferredSuffix() {
		return preferredSuffix;
	}

	public boolean isNameReleased() {
		return isNameReleased;
	}
	
	public final void setMothraId(String mothraId) {
		this.mothraId = mothraId;
	}

	public final void setPreferredFirstName(String preferredFirstName) {
		this.preferredFirstName = preferredFirstName;
	}

	public final void setPreferredLastName(String preferredLastName) {
		this.preferredLastName = preferredLastName;
	}

	public final void setPreferredMiddleName(String preferredMiddleName) {
		this.preferredMiddleName = preferredMiddleName;
	}

	public final void setPreferredSuffix(String preferredSuffix) {
		this.preferredSuffix = preferredSuffix;
	}

	public final void setNameReleased(boolean isNameReleased) {
		this.isNameReleased = isNameReleased;
	}
}
