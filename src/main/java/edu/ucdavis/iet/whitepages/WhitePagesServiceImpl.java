/*
 * Copyright 2008-2011 The Regents of the University of California
 *
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.opensource.org/licenses/ecl2.php
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.ucdavis.iet.whitepages;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

public class WhitePagesServiceImpl implements WhitePagesService
{
    /*
     * Injected Spring JDBC Template & White Pages Data Source
     */
    private JdbcTemplate jdbcTemplate;

    public void setWhitePagesDataSource(DataSource whitePagesDataSource)
    {
        this.jdbcTemplate = new JdbcTemplate(whitePagesDataSource);    
    }

    private String displayNameQuery = "select MOTHRAID, PREFERRED_FNAME, PREFERRED_MNAME, PREFERRED_LNAME, PREFERRED_SUFFIX, RELEASE_NAME FROM org.people_display_name WHERE mothraid = ?";

    public WhitePagesPerson findByPrimaryId(String primaryId)
    {
        WhitePagesPerson whitePagesPerson = (WhitePagesPerson) this.jdbcTemplate.queryForObject(displayNameQuery,
                new Object[] { primaryId }, new RowMapper() {
                    public Object mapRow(ResultSet rs, int rowNum) throws SQLException
                    {
                        WhitePagesPersonImpl whitePagesPerson = new WhitePagesPersonImpl();
                        whitePagesPerson.setMothraId(rs.getString("MOTHRAID"));
                        whitePagesPerson.setPreferredFirstName(rs.getString("PREFERRED_FNAME"));
                        whitePagesPerson.setPreferredMiddleName(rs.getString("PREFERRED_MNAME"));
                        whitePagesPerson.setPreferredLastName(rs.getString("PREFERRED_LNAME"));
                        whitePagesPerson.setNameReleased(rs.getString("RELEASE_NAME").equals("Y") ? true : false);
                        return whitePagesPerson;
                    }
                });
        return whitePagesPerson;
    }
}
