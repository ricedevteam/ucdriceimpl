/*
 * Copyright 2008-2011 The Regents of the University of California
 *
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.opensource.org/licenses/ecl2.php
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.ucdavis.kuali.rice.core.service.impl;

import java.security.GeneralSecurityException;

import org.apache.commons.lang.StringUtils;
import org.kuali.rice.core.api.config.property.ConfigContext;
//import org.kuali.rice.core.service.EncryptionService;
import org.kuali.rice.core.api.encryption.EncryptionService;

import edu.ucdavis.iet.commons.utils.encryption.UcEncryption;

/**
 * UC Encryption service implementation using AES 128bit key for
 * encrypting and decrypting. *
 */

public class UcEncryptionServiceImpl implements EncryptionService {

    private transient String key;
    private boolean isEnabled = false;

    public UcEncryptionServiceImpl() throws Exception {
        if (key != null) {
            throw new RuntimeException("The secret key must be kept secret. Storing it in the Java source code is a really bad idea.");
        }
        String propKey = ConfigContext.getCurrentContextConfig().getProperty("encryption.key");
        if (!StringUtils.isEmpty(propKey)) {
            key = propKey;
            isEnabled = true;
        }
    }
    
    public boolean isEnabled() {
        return isEnabled;
    }

    public String encrypt(Object valueToHide) throws GeneralSecurityException {
        return UcEncryption.encrypt(key, valueToHide);
    }

    public String decrypt(String ciphertext) throws GeneralSecurityException {
        return UcEncryption.decrypt(key, ciphertext);
    }

    public byte[] encryptBytes(byte[] valueToHide) throws GeneralSecurityException {
        return UcEncryption.encryptBytes(key, valueToHide);
        
    }

    public byte[] decryptBytes(byte[] ciphertext) throws GeneralSecurityException {
        return UcEncryption.decryptBytes(key, ciphertext);
    }

    public String hash(Object valueToHide) throws GeneralSecurityException {
        return UcEncryption.hash(valueToHide);
    }
}