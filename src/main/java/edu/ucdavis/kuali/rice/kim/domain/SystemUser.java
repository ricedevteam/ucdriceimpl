/*
 * Copyright 2008-2011 The Regents of the University of California
 *
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.opensource.org/licenses/ecl2.php
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.ucdavis.kuali.rice.kim.domain;

public class SystemUser
{
    private String systemUserId;
    private String systemuserName;
    private String systemUserEntityId;

    public void setSystemUserId(String systemUserId)
    {
        this.systemUserId = systemUserId;
    }
    public String getSystemUserId()
    {
        return systemUserId;
    }
    public void setSystemUserName(String systemuserName)
    {
        this.systemuserName = systemuserName;
    }
    public String getSystemUserName()
    {
        return systemuserName;
    }
    public void setSystemUserEntityId(String systemUserEntityId)
    {
        this.systemUserEntityId = systemUserEntityId;
    }
    public String getSystemUserEntityId()
    {
        return systemUserEntityId;
    }

}
