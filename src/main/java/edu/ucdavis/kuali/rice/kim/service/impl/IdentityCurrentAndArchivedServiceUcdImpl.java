/*
 * Copyright 2008-2011 The Regents of the University of California
 *
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.opensource.org/licenses/ecl2.php
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * UC Davis implementation of the Identity Current and Archived Service. 
 *
 * @author eldavid
 *
 */
package edu.ucdavis.kuali.rice.kim.service.impl;

import org.apache.log4j.Logger;
import org.kuali.rice.kim.api.identity.principal.Principal;
import org.kuali.rice.kim.api.identity.entity.EntityDefault;
import org.kuali.rice.kim.api.identity.IdentityService;
// import org.kuali.rice.kim.api.services.KimApiServiceLocator;
import org.kuali.rice.kim.impl.identity.IdentityArchiveService;

public class IdentityCurrentAndArchivedServiceUcdImpl extends org.kuali.rice.kim.impl.identity.IdentityCurrentAndArchivedServiceImpl
{
    private final IdentityArchiveService identityArchiveService;
    private final IdentityService innerIdentityService;
    private static final Logger LOG = Logger.getLogger(IdentityCurrentAndArchivedServiceUcdImpl.class);
    
    /**
     * This constructs a IdentityCurrentAndArchivedServiceImpl, injecting the
     * needed services.
     */
    public IdentityCurrentAndArchivedServiceUcdImpl(IdentityService innerIdentityService, IdentityArchiveService identityArchiveService)
    {
        super(innerIdentityService, identityArchiveService);
        this.innerIdentityService = innerIdentityService;
        this.identityArchiveService = identityArchiveService;
    }
    
    // TODO KR-529
    @Override
    /**
     * @see org.kuali.rice.kim.service.IdentityService#getPrincipal(java.lang.String)
     */
    public Principal getPrincipal(String principalId) 
    {
        LOG.debug("getPrincipal(): Searching for Principal with ID: " + principalId + ".");
        Principal kimPrincipalInfo = getInnerIdentityService().getPrincipal(principalId);
        if (kimPrincipalInfo == null)
        {
            LOG.debug("getPrincipal(): Inner Identity Service did not return a Principal with ID: " + principalId + ".");
            // If a principal is not found through the Identity Service, find it through the Identity Archive Service
            EntityDefault entityDefaultInfo = getIdentityArchiveService().getEntityDefaultFromArchiveByPrincipalId(principalId);
            if (entityDefaultInfo == null)
            {
                // If still not found via Identity Archive Service, return null.
                LOG.debug("getPrincipal(): Identity Archive Service did not return a Principal with ID: " + principalId + ".");
                return null;
            }
            /*
             *  Traverse the list of Principals belonging to this Entity and get the one that matches
             *  the passed-in Principal ID. There should only be one if at all.
             */    
            LOG.debug("getPrincipal(): Identity Archive Service returned a Principal with ID:" + principalId + ".");
            for (Principal principal : entityDefaultInfo.getPrincipals())
            {
                if (principal.getPrincipalId().equals(principalId))
                {
                    LOG.debug("getPrincipal(): Building KimPrincipalImpl from KimEntityDefaultInfo.");

  //                  Principal principalContract = KimApiServiceLocator.getIdentityService().getPrincipal(principalId);
                    Principal.Builder principalBuilder = Principal.Builder.create(principal.getPrincipalName());
                    principalBuilder.setEntityId(principal.getEntityId());
                    principalBuilder.setPrincipalId(principal.getPrincipalId());
                    principalBuilder.setPrincipalName(principal.getPrincipalName());
                    kimPrincipalInfo = principalBuilder.build();
                }
            }
        }
        else
        {
            LOG.debug("getPrincipal(): Identity Service returned a Principal with ID:" + principalId + ".");
        }
        return kimPrincipalInfo;
    }
    
    // TODO KR-529
    @Override
    /**
     * @see org.kuali.rice.kim.service.IdentityService#getPrincipalByPrincipalName(java.lang.String)
     */
    public Principal getPrincipalByPrincipalName(String principalName) 
    {
        LOG.debug("getPrincipalByPrincipalName(): Searching for Principal with Name: " + principalName + ".");
        Principal kimPrincipalInfo = getInnerIdentityService().getPrincipalByPrincipalName(principalName);
        if (kimPrincipalInfo == null)
        {
            LOG.debug("getPrincipalByPrincipalName(): Inner Identity Service did not return a Principal with ID: " + principalName + ".");
            // If a principal is not found through the Identity Service, find it through the Identity Archive Service
            EntityDefault entityDefaultInfo = getIdentityArchiveService().getEntityDefaultFromArchiveByPrincipalName(principalName);
            if (entityDefaultInfo == null)
            {
                // If still not found via Identity Archive Service, return null.
                LOG.debug("getPrincipalByPrincipalName(): Identity Archive Service did not return a Principal with ID: " + principalName + ".");
                return null;
            }
            /*
             *  Traverse the list of Principals belonging to this Entity and get the one that matches
             *  the passed-in Principal ID. There should only be one if at all.
             */    
            LOG.debug("getPrincipalByPrincipalName(): Identity Archive Service returned a Principal with ID:" + principalName + ".");
            for (Principal principal : entityDefaultInfo.getPrincipals())
            {
                if (principal.getPrincipalId().equals(principalName))
                {
                    LOG.debug("getPrincipalByPrincipalName(): Building KimPrincipalImpl from KimEntityDefaultInfo.");

//                    Principal principalContract = KimApiServiceLocator.getIdentityService().getPrincipalByPrincipalName(principalName);
                    Principal.Builder principalBuilder = Principal.Builder.create(principalName);
                    principalBuilder.setEntityId(principal.getEntityId());
                    principalBuilder.setPrincipalId(principal.getPrincipalId());
                    principalBuilder.setPrincipalName(principal.getPrincipalName());
                    kimPrincipalInfo = principalBuilder.build();
                }
            }
        }
        else
        {
            LOG.debug("getPrincipal(): Identity Service returned a Principal with ID:" + principalName + ".");
        }
        return kimPrincipalInfo;
    }
    
    private IdentityService getInnerIdentityService()
    {
        return innerIdentityService;
    }
    
    private IdentityArchiveService getIdentityArchiveService()
    {
        return identityArchiveService;
    }
}
