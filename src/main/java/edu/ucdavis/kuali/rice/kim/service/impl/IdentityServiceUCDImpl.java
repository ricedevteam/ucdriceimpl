/*
 * Copyright 2008-2011 The Regents of the University of California
 *
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.opensource.org/licenses/ecl2.php
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * UC Davis implementation of the Identity Service. This version assumes that
 * KIM Entities and related entity objects are located in the UCD LDAP Server.
 *
 * @author eldavid
 *
 */
package edu.ucdavis.kuali.rice.kim.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import org.kuali.rice.kim.impl.KIMPropertyConstants;
import org.kuali.rice.kim.api.identity.IdentityService;
import org.kuali.rice.kim.impl.identity.IdentityServiceImpl;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.ldap.LimitExceededException;
import org.springframework.ldap.NameNotFoundException;

import org.kuali.rice.kim.api.identity.affiliation.EntityAffiliation;
import org.kuali.rice.kim.api.identity.affiliation.EntityAffiliationType;
import org.kuali.rice.kim.api.identity.address.EntityAddress;
import org.kuali.rice.kim.api.identity.email.EntityEmail;
import org.kuali.rice.kim.api.identity.phone.EntityPhone;
import org.kuali.rice.kim.api.identity.entity.Entity;
import org.kuali.rice.kim.api.identity.entity.Entity.Builder;
import org.kuali.rice.kim.api.identity.entity.EntityDefault;
import org.kuali.rice.kim.api.identity.type.EntityTypeContactInfo;
import org.kuali.rice.kim.api.identity.name.EntityName;
import org.kuali.rice.kim.api.identity.principal.EntityNamePrincipalName;
import org.kuali.rice.kim.api.identity.principal.Principal;
import org.kuali.rice.kim.api.identity.privacy.EntityPrivacyPreferences;
import org.kuali.rice.kim.api.identity.employment.EntityEmployment;

import edu.ucdavis.iet.commons.ldap.domain.LdapAccount;
import edu.ucdavis.iet.commons.ldap.domain.LdapMailListing;
import edu.ucdavis.iet.commons.ldap.domain.LdapPerson;
import edu.ucdavis.iet.commons.ldap.domain.util.LdapAccountConstants;
import edu.ucdavis.iet.commons.ldap.domain.util.LdapPersonConstants;
import edu.ucdavis.iet.commons.ldap.service.LdapAccountService;
import edu.ucdavis.iet.commons.ldap.service.LdapMailListingService;
import edu.ucdavis.iet.commons.ldap.service.LdapPersonService;
import edu.ucdavis.iet.whitepages.WhitePagesPerson;
import edu.ucdavis.iet.whitepages.WhitePagesService;
import edu.ucdavis.iet.whitepages.WhitePagesServiceException;
import edu.ucdavis.kuali.rice.kim.domain.SystemUser;

public class IdentityServiceUCDImpl extends IdentityServiceImpl implements IdentityService
{
    private static final Logger LOG = Logger.getLogger(IdentityServiceUCDImpl.class);

    /*
     * Injected System Entities with Spring (since they will NOT be on LDAP)
     */
    private List<SystemUser> systemUserList ;

    public void setSystemUserList(List<SystemUser> systemUserList)
    {
        this.systemUserList = systemUserList;
    }

    /*
     * Injected LDAP Account Service
     */
    private LdapAccountService ldapAccountService;

    public void setLdapAccountService(LdapAccountService ldapAccountService)
    {
        this.ldapAccountService = ldapAccountService;
    }

    /*
     * Injected LDAP Mail Listing Service
     */
    private LdapMailListingService ldapMailListingService;
    public void setLdapMailListingService(LdapMailListingService ldapMailListingService)
    {
        this.ldapMailListingService = ldapMailListingService;
    }

    /*
     * Injected LDAP Person Service
     */
    private LdapPersonService ldapPersonService;
    public void setLdapPersonService(LdapPersonService ldapPersonService)
    {
        this.ldapPersonService = ldapPersonService;
    }

    /*
     * Injected White Pages Service
     */
    private WhitePagesService whitePagesService;

    public void setWhitePagesService(WhitePagesService whitePagesService)
    {
        this.whitePagesService = whitePagesService;
    }

    /*
     * Criteria conversion constructs
     */
    private static final String PRINCIPAL_PROPERTY_PREFIX = "principals.";
    // TODO Revisit "kimPersonServiceKeyConversion"
    protected Map<String, String> kimPersonServiceKeyConversion = new HashMap<String, String>();
    protected Map<String, String> ucdKimEntityKeyConversion = new HashMap<String, String>();
    {
        /*
         * Conversion map of KIM Person Service search criteria keys to UCD IdM
         * search criteria keys
         */
        kimPersonServiceKeyConversion.put(KIMPropertyConstants.Person.ENTITY_ID, LdapPersonConstants.UNIVERSAL_USER_ID);
        kimPersonServiceKeyConversion
                .put(PRINCIPAL_PROPERTY_PREFIX + KIMPropertyConstants.Person.PRINCIPAL_ID, LdapAccountConstants.USER_ID_NUMBER);
        kimPersonServiceKeyConversion.put(PRINCIPAL_PROPERTY_PREFIX + KIMPropertyConstants.Person.PRINCIPAL_NAME,
                LdapPersonConstants.USER_ID);
        kimPersonServiceKeyConversion.put("names.firstName", LdapPersonConstants.FIRST_NAME);
        kimPersonServiceKeyConversion.put("names.lastName", LdapPersonConstants.LAST_NAME);
        kimPersonServiceKeyConversion.put("entityTypes.emailAddresses.emailAddress", LdapPersonConstants.EMAIL_ADDRESS);
        kimPersonServiceKeyConversion.put("entityTypes.phoneNumbers.phoneNumber", LdapPersonConstants.PHONE_NUMBER);
        kimPersonServiceKeyConversion.put("entityTypes.addresses.line1", LdapPersonConstants.STREET);
        kimPersonServiceKeyConversion.put("entityTypes.addresses.cityName", LdapPersonConstants.CITY_NAME);
        kimPersonServiceKeyConversion.put("entityTypes.addresses.stateCode", LdapPersonConstants.STATE_CODE);
        kimPersonServiceKeyConversion.put("entityTypes.addresses.postalCode", LdapPersonConstants.POSTAL_CODE);
        kimPersonServiceKeyConversion.put("affiliations.affiliationTypeCode", LdapPersonConstants.AFFILIATION_TYPE_CODE);
        kimPersonServiceKeyConversion.put("employmentInformation.employeeTypeCode", LdapPersonConstants.AFFILIATION_TYPE_CODE);
        kimPersonServiceKeyConversion.put("employmentInformation.employeeId", LdapPersonConstants.EMPLOYEE_ID);
        kimPersonServiceKeyConversion.put("employmentInformation.primaryDepartmentCode", LdapPersonConstants.PRIMARY_DEPARTMENT_CODE);
        /*
         * Conversion map of UCD KIM Entity search criteria keys to UCD IdM
         * search criteria keys
         */
        ucdKimEntityKeyConversion.put(UCDKIMEntitySearchKeys.Person.ENTITY_ID, LdapPersonConstants.UNIVERSAL_USER_ID);
        ucdKimEntityKeyConversion.put(UCDKIMEntitySearchKeys.Person.PRINCIPAL_ID, LdapAccountConstants.USER_ID_NUMBER);
        ucdKimEntityKeyConversion.put(UCDKIMEntitySearchKeys.Person.PRINCIPAL_NAME, LdapPersonConstants.USER_ID);
        ucdKimEntityKeyConversion.put(UCDKIMEntitySearchKeys.Person.FIRST_NAME, LdapPersonConstants.FIRST_NAME);
        ucdKimEntityKeyConversion.put(UCDKIMEntitySearchKeys.Person.LAST_NAME, LdapPersonConstants.LAST_NAME);
        ucdKimEntityKeyConversion.put(UCDKIMEntitySearchKeys.Person.EMAIL_ADDRESS, LdapPersonConstants.EMAIL_ADDRESS);
        ucdKimEntityKeyConversion.put(UCDKIMEntitySearchKeys.Person.PHONE_NUMBER, LdapPersonConstants.PHONE_NUMBER);
        ucdKimEntityKeyConversion.put(UCDKIMEntitySearchKeys.Person.ADDRESS_LINE_1, LdapPersonConstants.STREET);
        ucdKimEntityKeyConversion.put(UCDKIMEntitySearchKeys.Person.CITY_NAME, LdapPersonConstants.CITY_NAME);
        ucdKimEntityKeyConversion.put(UCDKIMEntitySearchKeys.Person.STATE_CODE, LdapPersonConstants.STATE_CODE);
        ucdKimEntityKeyConversion.put(UCDKIMEntitySearchKeys.Person.POSTAL_CODE, LdapPersonConstants.POSTAL_CODE);
        ucdKimEntityKeyConversion.put(UCDKIMEntitySearchKeys.Person.AFFILIATION_TYPE_CODE, LdapPersonConstants.UCD_AFFILIATION_TYPE_CODE);
        ucdKimEntityKeyConversion.put(UCDKIMEntitySearchKeys.Person.EMPLOYEE_TYPE_CODE, LdapPersonConstants.AFFILIATION_TYPE_CODE);
        ucdKimEntityKeyConversion.put(UCDKIMEntitySearchKeys.Person.EMPLOYEE_ID, LdapPersonConstants.EMPLOYEE_ID);
        ucdKimEntityKeyConversion.put(UCDKIMEntitySearchKeys.Person.PRIMARY_DEPARTMENT_CODE, LdapPersonConstants.PRIMARY_DEPARTMENT_CODE);
    }

    /**
     * Find an LDAP Person by Entity ID and return an Entity
     *
     * @see org.kuali.rice.kim.service.impl.IdentityServiceImpl#getEntityImpl(java.lang.String)
     */
    @Override
    public Entity getEntity(String entityId)
    {
        if (StringUtils.isBlank(entityId))
        {
            return null;
        }

        /* use spring list first */
        for (SystemUser user : systemUserList)
        {
            if (user.getSystemUserEntityId().equalsIgnoreCase(entityId))
            {
            	return composeEntityForSystemUser(user).build();
            }
        }

        try
        {
            return composeEntityFromLdapPerson(ldapPersonService.findByUniversalUserId(entityId));
        }
        catch (NameNotFoundException nnfe)
        {
        	// If the Entity is not found, call the baseline implementation
        	if (LOG.isDebugEnabled())
            {
        		LOG.debug("getEntityImpl() caught NameNotFoundException on: " + entityId);
            }
            return super.getEntity(entityId);
        }

    }

    /**
     * Find an LDAP Person by Principal ID and return an Entity
     *
     * @see org.kuali.rice.kim.service.impl.IdentityServiceImpl#getEntityByPrincipalId(java.lang.String)
     */
    @Override
    public Entity getEntityByPrincipalId(String principalId)
    {
        if (StringUtils.isBlank(principalId))
        {
            return null;
        }
        /* use spring list first */
        for (SystemUser user : systemUserList)
        {
            if (user.getSystemUserId().equalsIgnoreCase(principalId))
            {
                return composeEntityForSystemUser(user).build();
            }
        }
        try
        {
            return composeEntityFromLdapPerson(ldapPersonService.findByUserId(ldapAccountService.findByUserIdNumber(principalId, "IKRB").getUid()));
        }
        catch (NameNotFoundException nnfe)
        {
            if (LOG.isDebugEnabled())
            {
            	LOG.debug("getEntityByPrincipalId() caught NameNotFoundException on: " + principalId);
            }
            return super.getEntityByPrincipalId(principalId);
        }
        catch (EmptyResultDataAccessException erdae)
        {
        	if (LOG.isDebugEnabled())
            {
        		LOG.debug("getEntityByPrincipalId() caught EmptyResultDataAccessException on: " + principalId);
            }
        	return super.getEntityByPrincipalId(principalId);
        }
        // KR-599: band-aid fix for multiple records in ou=Accounts which have the same Principal ID. Get the active record from cache.
        catch (IncorrectResultSizeDataAccessException irsde)
        {
        	LOG.error("getEntityByPrincipalId() caught IncorrectResultSizeDataAccessException on: " + principalId);
           	List<LdapAccount> ldapAccounts = ldapAccountService.findAccountsByUserIDNumber(principalId);
            Entity.Builder entityBuilder = Entity.Builder.create();

            for (LdapAccount ldapAccount : ldapAccounts)
        	{
        		String uid = ldapAccount.getUid();
        		try
        		{
	        		LdapPerson ldapPerson = ldapPersonService.findByUserId(uid);
	        		if (ldapPerson != null)
	        		{
	                    entityBuilder.setId(ldapPerson.getUcdPersonUUID());
	                    entityBuilder.setActive(true);
	                    Principal.Builder principalBuilder = Principal.Builder.create(uid);
	                    principalBuilder.setActive(true);
	                    principalBuilder.setPrincipalId(principalId);
	                    principalBuilder.setPrincipalName(uid);
	                    principalBuilder.setEntityId(ldapPerson.getUcdPersonUUID());
	                    
	                    List<Principal.Builder> principals = new ArrayList<Principal.Builder>();
	                    principals.add(principalBuilder);
	                    entityBuilder.setPrincipals(principals);
	                    break;
	        		}
        		}
        		catch (Exception e)
        		{

        		}
        	}
            return entityBuilder.build();
        }
    }

    /**
     * Find an LDAP Person by Principal Name and return an Entity
     *
     * @see org.kuali.rice.kim.service.impl.IdentityServiceImpl#getEntityByPrincipalName(java.lang.String)
     */
    @Override
    public Entity getEntityByPrincipalName(String principalName)
    {
        if (StringUtils.isBlank(principalName))
        {
            return null;
        }
        /* use spring list first */
        for (SystemUser user : systemUserList)
        {
            if (user.getSystemUserName().equalsIgnoreCase(principalName))
            {
                return composeEntityForSystemUser(user).build();
            }
        }

        try
        {
            return composeEntityFromLdapPerson(ldapPersonService.findByUserId(principalName));
        }
        catch (NameNotFoundException nnfe)
        {
        	if (LOG.isDebugEnabled())
            {
        		LOG.debug("getEntityByPrincipalName() caught NameNotFoundException on: " + principalName);
            }
        	return super.getEntityByPrincipalName(principalName);
        }
        catch (EmptyResultDataAccessException erdae)
        {
        	if (LOG.isDebugEnabled())
            {
        		LOG.debug("getEntityByPrincipalName() caught EmptyResultDataAccessException on: " + principalName);
            }
        	return super.getEntityByPrincipalName(principalName);
        }
    }

    /**
     * Find a list of LDAP Persons and return a list of default information of
     * Entitys. Primarily used by the KIM Person Lookup page.
     *
     * @see org.kuali.rice.kim.service.IdentityService#lookupEntityDefaultInfo(Map,boolean)
     * @deprecated
     * @TODO Replace with public EntityQueryResults findEntityDefaults (QueryByCriteria queryByCriteria)
     */
//    @Override - 
    public List<EntityDefault> lookupEntityDefaultInfo(Map<String, String> searchCriteria, boolean unbounded)
    {
        if (searchCriteria == null || searchCriteria.isEmpty())
        {
            return null;
        }
        if (LOG.isDebugEnabled())
        {
            for (String searchKey : searchCriteria.keySet())
            {
                LOG.debug("Search Key: " + searchKey + " - Criteria: " + searchCriteria.get(searchKey));
            }
        }
        List<Entity> entityList = null;
        try
        {
        	if (LOG.isDebugEnabled())
            {
        		LOG.debug("lookupEntityDefaultInfo() calling lookupEntitys()");
            }
            entityList = lookupEntitys(convertSearchKeys(searchCriteria));
        }
        catch (IllegalArgumentException iae)
        {
        	if (LOG.isDebugEnabled())
            {
        		LOG.debug("lookupEntityDefaultInfo() caught IllegalArgumentException: " + iae.getMessage() + "; returning empty ArrayList<KimEntityDefaultInfo>().");
            }
            return new ArrayList<EntityDefault>();
        }
        if (entityList == null)
        {
        	if (LOG.isDebugEnabled())
            {
        		LOG.debug("lookupEntityDefaultInfo() was returned a null Entity List.");
            }
            // return super.lookupEntityDefaultInfo(searchCriteria, unbounded);
            return new ArrayList<EntityDefault>();
        }
        if (LOG.isDebugEnabled())
        {
    		LOG.debug("entityList size is: " + entityList.size());
        }
        List<EntityDefault> entityDefaultInfoList = new ArrayList<EntityDefault>(entityList.size());
        for (Entity entity : entityList)
        {
        	if (LOG.isDebugEnabled())
            {
        		LOG.debug("Fetched Entity ID:: " + entity.getId());
            }
            entityDefaultInfoList.add(getEntityDefault(entity.getId()));
        }
        return entityDefaultInfoList;
    }

    /**
     * Find a list of LDAP Persons and return a list of Entitys
     *
     * @see org.kuali.rice.kim.service.impl.IdentityServiceImpl#lookupEntitys(Map)
     * @deprecated 
     * @TODO Replace with public EntityQueryResults findEntities (QueryByCriteria queryByCriteria)
     */
//    @Override
    public List<Entity> lookupEntitys(Map<String, String> searchCriteria)
    {
        if (searchCriteria == null || searchCriteria.isEmpty())
        {
            return null;
        }
        // If an E-mail Address key is found in the search criteria, find the
        // Entity by first using the LDAP Mail Listing Service
        if (searchCriteria.containsKey(UCDKIMEntitySearchKeys.Person.EMAIL_ADDRESS))
        {
            // Find all Universal User IDs that have the given e-mail address in the ou=Listings schema
        	if (LOG.isDebugEnabled())
            {
        		LOG.debug("lookupEntitys() searching by E-mail Address: " + searchCriteria.get(UCDKIMEntitySearchKeys.Person.EMAIL_ADDRESS));
            }
            List<LdapMailListing> ldapMailListings = null;
            try
            {
                ldapMailListings = ldapMailListingService.findByEmailAddress(searchCriteria.get(UCDKIMEntitySearchKeys.Person.EMAIL_ADDRESS));
            }
            // If there are no listings in the ou=Listings schema, do nothing. Move on to the regular search.
            catch (NameNotFoundException nnfe)
            {
            	if (LOG.isDebugEnabled())
                {
            		LOG.debug("lookupEntitys() search by E-mail Address found no matches.");
                }
            }
            // Compose a list of Entitys from the set of returned Universal User IDs in the list of LDAP Mail Listings
            if (ldapMailListings != null)
            {
            	if (LOG.isDebugEnabled())
                {
            		LOG.debug("lookupEntitys() search by E-mail Address found a match.");
                }
                List<Entity> kimEntityList = new ArrayList<Entity>();
                for  (LdapMailListing ldapMailListing : ldapMailListings)
                {
                    kimEntityList.add(getEntity(ldapMailListing.getUcdPersonUUID()));
                }
                return kimEntityList;
            }
        }
        // If a Principal Id key is found in the search criteria, find it directly.
        if (searchCriteria.containsKey(LdapAccountConstants.USER_ID_NUMBER))
        {
        	if (LOG.isDebugEnabled())
            {
        		LOG.debug("lookupEntitys() searching by Principal ID: " + searchCriteria.get(LdapAccountConstants.USER_ID_NUMBER));
            }
            Entity kimEntity = getEntityByPrincipalId(searchCriteria.get(LdapAccountConstants.USER_ID_NUMBER));
            if (kimEntity != null)
            {
            	if (LOG.isDebugEnabled())
                {
            		LOG.debug("lookupEntitys() search by Principal ID found a match.");
                }
                List<Entity> kimEntityList = new ArrayList<Entity>();
                kimEntityList.add(kimEntity);
                return kimEntityList;
            }
            else
            {
            	if (LOG.isDebugEnabled())
                {
            		LOG.debug("lookupEntitys() search by Principal ID found no matches.");
                }
                return null;
            }
        }
        // If a Principal Name key is found in the search criteria, find it directly.
        if (searchCriteria.containsKey(LdapAccountConstants.USER_ID))
        {
        	if (LOG.isDebugEnabled())
            {
        		LOG.debug("lookupEntitys() searching by Principal Name: " + searchCriteria.get(LdapAccountConstants.USER_ID));
            }
            Entity kimEntity = getEntityByPrincipalName(searchCriteria.get(LdapAccountConstants.USER_ID));
            if (kimEntity != null)
            {
                LOG.debug("lookupEntitys() search by Principal Name found a match.");
                List<Entity> kimEntityList = new ArrayList<Entity>();
                kimEntityList.add(kimEntity);
                return kimEntityList;
            }
            else
            {
            	if (LOG.isDebugEnabled())
                {
            		LOG.debug("lookupEntitys() search by Principal Name found no matches.");
                }
                return null;
            }
        }
        // If an Entity ID key is found in the search criteria, find it directly.
        if (searchCriteria.containsKey(LdapPersonConstants.UNIVERSAL_USER_ID))
        {
        	if (LOG.isDebugEnabled())
            {
        		LOG.debug("lookupEntitys() searching by Entity ID: " + searchCriteria.get(LdapPersonConstants.UNIVERSAL_USER_ID));
            }
            Entity kimEntity = getEntity(searchCriteria.get(LdapPersonConstants.UNIVERSAL_USER_ID));
            if (kimEntity != null)
            {
            	if (LOG.isDebugEnabled())
                {
            		LOG.debug("lookupEntitys() search by Entity ID found a match.");
                }
                List<Entity> kimEntityList = new ArrayList<Entity>();
                kimEntityList.add(kimEntity);
                return kimEntityList;
            }
            else
            {
            	if (LOG.isDebugEnabled())
                {
            		LOG.debug("lookupEntitys() search by Entity ID found no matches.");
                }
                return null;
            }
        }
        // Intercept Employee ID criterion and perform a non-wildcard search solely on Employee ID
        if (searchCriteria.containsKey(LdapPersonConstants.EMPLOYEE_ID))
        {
        	LOG.debug("lookupEntitys() searching by Employee ID: " + searchCriteria.get(LdapPersonConstants.EMPLOYEE_ID));
        	Map<String,String> employeeIdCriterion = new HashMap<String,String>();
        	employeeIdCriterion.put(LdapPersonConstants.EMPLOYEE_ID, searchCriteria.get(LdapPersonConstants.EMPLOYEE_ID));
        	List<Entity> kimEntity = composeEntitysFromLdapPersons(ldapPersonService.findByCriteria(employeeIdCriterion));
            if (kimEntity != null)
            {
                LOG.debug("lookupEntitys() search by Employee ID found a match.");
                return kimEntity;
            }
            else
            {
                LOG.debug("lookupEntitys() search by Employee ID found no matches.");
                return null;
            }
        }
        // Perform a regular search using the LDAP Person Service
        if (LOG.isDebugEnabled())
        {
    		LOG.debug("lookupEntitys() search criteria requires performing a regular search.");
        }
        try
        {
            List<Entity> kimEntityList = composeEntitysFromLdapPersons(ldapPersonService.findByCriteria(searchCriteria,  false));
            if (LOG.isDebugEnabled())
            {
        		LOG.debug("lookupEntitys() regular search found one or more matches.");
            }
            return kimEntityList;
        }
        catch (EmptyResultDataAccessException erdae)
        {
            LOG.error("lookupEntitys() caught EmptyResultDataAccessException on: " + searchCriteria.toString());
            return null;
        }
        catch (NameNotFoundException nnfe)
        {
            LOG.error("lookupEntitys() caught NameNotFoundException on: " + searchCriteria.toString());
            return null;
        }
        catch (IllegalArgumentException iae)
        {
            LOG.error("lookupEntitys() caught IllegalArgumentException on: " + searchCriteria.toString());
            return null;
        }
        catch (LimitExceededException lee)
        {
            LOG.error("lookupEntitys() caught LimitExceededException on: " + searchCriteria.toString());
            return null;
        }
    }

    /**
     * Get the number of Entities that match the search criteria
     *
     * @see org.kuali.rice.kim.service.IdentityService#getMatchingEntityCount(java.util.Map)
     */
//    @Override
    public int getMatchingEntityCount(Map<String, String> searchCriteria)
    {
        if (searchCriteria == null || searchCriteria.isEmpty())
        {
            return 0;
        }
        int matchingEntityCount = 0;
        try
        {
            List<LdapPerson> ldapPersonList = ldapPersonService.findByCriteria(convertSearchKeys(searchCriteria));
            if (ldapPersonList != null)
            {
                matchingEntityCount = ldapPersonList.size();
            }
        }
        catch (EmptyResultDataAccessException erdae)
        {
        	LOG.error("getMatchingEntityCount() caught EmptyResultDataAccessException on: " + searchCriteria.toString());
        	return 0;
        }
        catch (IllegalArgumentException iae)
        {
        	LOG.error("getMatchingEntityCount() caught IllegalArgumentException on: " + searchCriteria.toString());
        	return 0;
        }
        catch (LimitExceededException lee)
        {
            LOG.error("getMatchingEntityCount() caught LimitExceededException on: " + searchCriteria.toString());
            return 0;
        }
        return matchingEntityCount;
    }

    /**
     * Find an LDAP Person by Principal ID and return a Principal
     *
     * @see org.kuali.rice.kim.service.impl.IdentityServiceImpl#getPrincipalImpl(java.lang.String)
     */
    @Override
    public Principal getPrincipal(String principalId)
    {
        if (StringUtils.isBlank(principalId))
        {
            return null;
        }

//        Principal principalContract = KimApiServiceLocator.getIdentityService().getPrincipal(principalId);
        Principal.Builder principalBuilder = Principal.Builder.create(principalId);
        
        /* use spring list first */
        for (SystemUser user : systemUserList)
        {
            if (user.getSystemUserId().equalsIgnoreCase(principalId))
            {
                principalBuilder.setActive(true);
                principalBuilder.setEntityId(user.getSystemUserEntityId());
                principalBuilder.setPrincipalId(user.getSystemUserId());
                principalBuilder.setPrincipalName(user.getSystemUserName());
                return principalBuilder.build();
            }
        }
        try
        {
            LdapAccount ldapAccount = ldapAccountService.findByUserIdNumber(principalId, "IKRB");
            LdapPerson ldapPerson = ldapPersonService.findByUserId(ldapAccount.getUid());
            principalBuilder.setActive(true);
            principalBuilder.setEntityId(ldapPerson.getUcdPersonUUID());
            principalBuilder.setPrincipalId(ldapAccount.getUidNumber());
            principalBuilder.setPrincipalName(ldapAccount.getUid());
            return principalBuilder.build();
        }
        catch (NameNotFoundException nnfe)
        {
        	if (LOG.isDebugEnabled())
        	{
        		LOG.error("getPrincipalImpl() caught NameNotFoundException on: " + principalId);
        	}
        	return super.getPrincipal(principalId);
        }
        catch (EmptyResultDataAccessException erdae)
        {
        	if (LOG.isDebugEnabled())
        	{
        		LOG.error("getPrincipalImpl() caught EmptyResultDataAccessException on: " + principalId);
        	}
    		return super.getPrincipal(principalId);
        }
        // band-aid fix for multiple records in ou=Accounts which have the same Principal ID. Get the active record from cache.
        catch (IncorrectResultSizeDataAccessException irsde)
        {
        	LOG.error("getPrincipalImpl() caught IncorrectResultSizeDataAccessException on: " + principalId);

        	List<LdapAccount> ldapAccounts = ldapAccountService.findAccountsByUserIDNumber(principalId);
        	for (LdapAccount ldapAccount : ldapAccounts)
        	{
        		String uid = ldapAccount.getUid();
        		try
        		{
	        		LdapPerson ldapPerson = ldapPersonService.findByUserId(uid);
	        		if (ldapPerson != null)
	        		{
	                    principalBuilder.setActive(true);
	                    principalBuilder.setPrincipalId(principalId);
	                    principalBuilder.setPrincipalName(uid);
	                    principalBuilder.setEntityId(ldapPerson.getUcdPersonUUID());
	                    break;
	        		}
        		}
        		catch (Exception e)
        		{

        		}
        	}
        	return principalBuilder.build();
        }
    }

    /**
     * Find an LDAP Person by Principal Name and return a Principal DTO
     *
     * @see org.kuali.rice.kim.service.IdentityService#getPrincipalByPrincipalName(java.lang.String)
     */
    @Override
    public Principal getPrincipalByPrincipalName(String principalName)
    {
        if (StringUtils.isBlank(principalName))
        {
            return null;
        }
        /* use spring list first */
        Principal.Builder principalBuilder = Principal.Builder.create(principalName);
        
        for (SystemUser user : systemUserList)
        {
            if (user.getSystemUserName().equalsIgnoreCase(principalName))
            {
                principalBuilder.setActive(true);
                principalBuilder.setEntityId(user.getSystemUserEntityId());
                principalBuilder.setPrincipalId(user.getSystemUserId());
                principalBuilder.setPrincipalName(user.getSystemUserName());
                return principalBuilder.build();
            }
        }
        try
        {
            LdapPerson ldapPerson = ldapPersonService.findByUserId(principalName);
            LdapAccount ldapAccount = ldapAccountService.findByUserId(principalName, "IKRB");
            principalBuilder.setActive(true);
            principalBuilder.setEntityId(ldapPerson.getUcdPersonUUID());
            principalBuilder.setPrincipalId(ldapAccount.getUidNumber());
            principalBuilder.setPrincipalName(ldapAccount.getUid());
            return principalBuilder.build();
        }
        // If the Principal is not found, call the baseline implementation
        catch (NameNotFoundException nnfe)
        {
        	if (LOG.isDebugEnabled())
        	{
        		LOG.warn("getPrincipalByPrincipalName() caught NameNotFoundException on: " + principalName);
        	}
            return super.getPrincipalByPrincipalName(principalName);
        }
        catch (EmptyResultDataAccessException erdae)
        {
        	if (LOG.isDebugEnabled())
        	{
        		LOG.warn("getPrincipalByPrincipalName() caught EmptyResultDataAccessException on: " + principalName);
        	}
        	return super.getPrincipalByPrincipalName(principalName);
        }
    }
    
    /**
     * Given a list of Principal IDs, find the default name of each Principal.
     * 
     * @see org.kuali.rice.kim.service.IdentityService#getDefaultNamesForPrincipalIds(java.util.List)
     */
    public Map<String, EntityNamePrincipalName> getDefaultNamesForPrincipalIds(List<String> principalIds) {
		
    	Map<String, EntityNamePrincipalName> result = new HashMap<String, EntityNamePrincipalName>(principalIds.size());
		// Find the default name of each Principal
		for(String principalId : principalIds) {
			EntityNamePrincipalName.Builder namePrincipal = null;
			
			Principal principalInfo = getPrincipal(principalId);
			if (principalInfo != null) {
            	EntityDefault entityDefault =  getEntityDefaultByPrincipalId(principalId);
            	EntityDefault.Builder entityDefaultBuilder = EntityDefault.Builder.create(entityDefault.getEntityId());
	  	        namePrincipal = EntityNamePrincipalName.Builder.create(principalInfo.getPrincipalName(),entityDefaultBuilder.getName());
				// Set Principal Name
				namePrincipal.setPrincipalName(principalInfo.getPrincipalName());
				// Set default Entity Name
				// EntityDefault entityDefaultInfo = getEntityDefault(principalInfo.getEntityId());
				namePrincipal.setDefaultName(entityDefaultBuilder.getName());
				// Add the Name Info to the result set
				result.put(principalId, namePrincipal.build());
			}
		}
		if (!result.isEmpty()) {
			return result;	
		}
		// If there are no results, call the baseline implementation
		else {
			for(String principalId : principalIds) {

				EntityNamePrincipalName namePrincipal = super.getDefaultNamesForPrincipalId(principalId);
				result.put(principalId, namePrincipal);
			}
			return result; 
		}		
	}

    /*
     * Take a list of LDAP Persons and compose a list of Entitys
     */
    private List<Entity> composeEntitysFromLdapPersons(List<LdapPerson> ldapPersons)
    {
        List<Entity> kimEntitys = new ArrayList<Entity>();
        for (LdapPerson ldapPerson : ldapPersons)
        {
            kimEntitys.add(composeEntityFromLdapPerson(ldapPerson));
        }
        return kimEntitys;
    }

    /*
     * Take an LDAP Person and compose a KIM Entity
     */
    private Entity composeEntityFromLdapPerson(LdapPerson ldapPerson)
    {
//    	Entity entity = KimApiServiceLocator.getIdentityService().getEntity(ldapPerson.getUcdPersonUUID());
        Entity.Builder entityBuilder = Entity.Builder.create();
        entityBuilder.setActive(true);
    	entityBuilder.setId(ldapPerson.getUcdPersonUUID());
    	entityBuilder.setPrivacyPreferences(getEntityPrivacyPreferencesBuilder(ldapPerson.getUcdPersonUUID()));

        // Set Principals
        if (ldapPerson.getUid() != null)
        {
        	entityBuilder.setPrincipals(composePrincipalBuildersFromLdapPerson(ldapPerson));
        }
        // Set Affiliations
        if ((ldapPerson.getEduPersonAffiliation() != null) || (ldapPerson.getUcdPersonAffiliation() != null))
        {
        	entityBuilder.setAffiliations(composeAffiliationsFromLdapPerson(ldapPerson));
        }
        // Set Entity Types
        entityBuilder.setEntityTypes(composeTypesFromLdapPerson(ldapPerson));
        // Set Entity Names
        if (ldapPerson.getSn() != null)
        {
            try
            {
            	entityBuilder.setNames(composeNamesFromLdapPerson(ldapPerson));
            }
            catch (WhitePagesServiceException wpse)
            {
                LOG.error(wpse.toString());
            }
        }
        // Set Employment Information
        if (ldapPerson.getEmployeeNumber() != null)
        {
        	entityBuilder.setEmploymentInformation(composeEmploymentInformationFromLdapPerson(ldapPerson));
        }
     // Set Privacy preferences
        if (ldapPerson.getUcdPublishItemFlag() != null)
        {
        	entityBuilder.setPrivacyPreferences(composePrivacyPreferenceFromLdapPerson(ldapPerson));
        }
        return entityBuilder.build();
    }

    /* 
     * Take an LDAP Person and compose Privacy Preferences
     */
    private EntityPrivacyPreferences.Builder composePrivacyPreferenceFromLdapPerson(LdapPerson ldapPerson)
    {
        if (ldapPerson.getUcdPublishItemFlag() == null)
        {
        return null;
        }
        EntityPrivacyPreferences.Builder kimPrivacy = EntityPrivacyPreferences.Builder.create(ldapPerson.getUcdPersonUUID());
        kimPrivacy.setEntityId(ldapPerson.getUcdPersonUUID());
        kimPrivacy.setSuppressAddress(ldapPerson.getUcdPublishAddress() != null && ldapPerson.getUcdPublishAddress().equalsIgnoreCase("N"));
        kimPrivacy.setSuppressEmail(ldapPerson.getUcdPublishMail() != null && ldapPerson.getUcdPublishMail().equalsIgnoreCase("N"));
        kimPrivacy.setSuppressName(ldapPerson.getUcdPublishPerson() != null && ldapPerson.getUcdPublishPerson().equalsIgnoreCase("N"));
        kimPrivacy.setSuppressPersonal(ldapPerson.getUcdPublishPerson() != null && ldapPerson.getUcdPublishPerson().equalsIgnoreCase("N"));
        kimPrivacy.setSuppressPhone(ldapPerson.getUcdPublishPhone() != null && ldapPerson.getUcdPublishPhone().equalsIgnoreCase("N"));

        return kimPrivacy;
    }

    /*
     * Take an LDAP Person and compose a list of KIM Principals
     */
    private List<Principal.Builder> composePrincipalBuildersFromLdapPerson(LdapPerson ldapPerson)
    {
        if (ldapPerson.getUid() == null)
        {
            return null;
        }
        List<Principal.Builder> kimPrincipalBuilders = new ArrayList<Principal.Builder>(ldapPerson.getUid().size());
        for (String uid : ldapPerson.getUid())
        {
            
            Principal.Builder principalBuilder = Principal.Builder.create(uid);
            try
            {
                LdapAccount ldapAccount = ldapAccountService.findByUserId(uid, "IKRB");
                principalBuilder.setActive(true);
                principalBuilder.setEntityId(ldapPerson.getUcdPersonUUID());
                principalBuilder.setPrincipalId(ldapAccount.getUidNumber());
                principalBuilder.setPrincipalName(ldapAccount.getUid());
            }
            catch (EmptyResultDataAccessException erdae)
            {
                continue;   //Skip people that have no principal id number
            }
            kimPrincipalBuilders.add(principalBuilder);
        }
        return kimPrincipalBuilders;
    }

    
    
    /*
     * Take an LDAP Person and compose a list of KIM Entity Affiliations
     */
    private List<EntityAffiliation.Builder> composeAffiliationsFromLdapPerson(LdapPerson ldapPerson)
    {
        if (ldapPerson.getEduPersonAffiliation() == null)
        {
            return null;
        }
        List<EntityAffiliation.Builder> kimEntityAffiliations = new ArrayList<EntityAffiliation.Builder>(ldapPerson.getEduPersonAffiliation()
                .size());

    	EntityAffiliation.Builder kimEntityAffiliation  = null;
        
        for (String eduPersonAffiliation : ldapPerson.getEduPersonAffiliation())
        {
        	kimEntityAffiliation = EntityAffiliation.Builder.create();
        	kimEntityAffiliation.setActive(true);
        	kimEntityAffiliation.setEntityId(ldapPerson.getUcdPersonUUID());
        	EntityAffiliationType.Builder entityAffiliationTypeBuilder = EntityAffiliationType.Builder.create(eduPersonAffiliation);
        	kimEntityAffiliation.setAffiliationType(entityAffiliationTypeBuilder);
        	kimEntityAffiliation.setDefaultValue(true);
        	kimEntityAffiliation.setCampusCode("DV");
            kimEntityAffiliations.add(kimEntityAffiliation);
        }
        
        if (ldapPerson.getUcdPersonAffiliation() != null)
        {

        	for (String ucdPersonAffiliation : ldapPerson.getUcdPersonAffiliation())
            {
        		kimEntityAffiliation = EntityAffiliation.Builder.create();
        		kimEntityAffiliation.setActive(true);
        		kimEntityAffiliation.setEntityId(ldapPerson.getUcdPersonUUID());
            	EntityAffiliationType.Builder entityAffiliationTypeBuilder = EntityAffiliationType.Builder.create(ucdPersonAffiliation);
            	kimEntityAffiliation.setAffiliationType(entityAffiliationTypeBuilder);
            	kimEntityAffiliation.setCampusCode("DV");
                kimEntityAffiliations.add(kimEntityAffiliation);
            }
        }
        return kimEntityAffiliations;
    }

    /*
     * Take an LDAP Person and compose a list of KIM Entity Types
     */
    private List<EntityTypeContactInfo.Builder> composeTypesFromLdapPerson(LdapPerson ldapPerson)
    {
        // Set Entity Types
    	List<EntityTypeContactInfo.Builder> kimEntityTypes = new ArrayList<EntityTypeContactInfo.Builder>();
    	
    	EntityTypeContactInfo.Builder entityType = EntityTypeContactInfo.Builder.create(ldapPerson.getUcdPersonUUID(), "PERSON");
        entityType.setActive(true);
        entityType.setEntityTypeCode("PERSON");
        entityType.setEntityId(ldapPerson.getUcdPersonUUID());
        // Set Entity Addresses
        if ((ldapPerson.getStreet() != null) || (ldapPerson.getL() != null) || (ldapPerson.getPostalCode() != null)
                || (ldapPerson.getSt() != null))
        {
            entityType.setAddresses(composeAddressesFromLdapPerson(ldapPerson));
        }
        // Set Entity E-mail Addresses
        if (ldapPerson.getMail() != null)
        {
            entityType.setEmailAddresses(composeEmailAddressesFromLdapPerson(ldapPerson));
        }
        // Set Entity Phone Numbers
        if (ldapPerson.getTelephoneNumber() != null)
        {
            entityType.setPhoneNumbers(composePhoneNumbersFromLdapPerson(ldapPerson));
        }
        kimEntityTypes.add(entityType);
        return kimEntityTypes;
    }

    /*
     * Take an LDAP Person and compose a list of KIM Entity Addresses
     */
    private List<EntityAddress> composeAddressesFromLdapPerson(LdapPerson ldapPerson)
    {
        // If all address components are null, return null
        if ((ldapPerson.getStreet() == null) && (ldapPerson.getL() == null) && (ldapPerson.getPostalCode() == null)
                && (ldapPerson.getSt() == null))
        {
            return null;
        }
        List<EntityAddress> kimEntityAddresses = new ArrayList<EntityAddress>();
        
        EntityAddress.Builder kimEntityAddress = EntityAddress.Builder.create(); 
        kimEntityAddress.setActive(true);
        kimEntityAddress.setDefaultValue(true);
        kimEntityAddress.setEntityId(ldapPerson.getUcdPersonUUID());
        // Set Street Address
        if (ldapPerson.getStreet() != null)
        {
        	kimEntityAddress.setLine1(ldapPerson.getStreet().get(0));
        }
        // Set City
        if ((ldapPerson.getL() != null))
        {
        	kimEntityAddress.setCity(ldapPerson.getL().get(0));
        }
        // Set State
        if (ldapPerson.getSt() != null)
        {
        	kimEntityAddress.setStateProvinceCode(ldapPerson.getSt().get(0));
        }
        // Set Postal Code
        if (ldapPerson.getPostalCode() != null)
        {
        	kimEntityAddress.setPostalCode(ldapPerson.getPostalCode().get(0));
        }
        kimEntityAddresses.add(kimEntityAddress.build());
        return kimEntityAddresses;
    }

    /*
     * Take an LDAP Person and compose a list of KIM Entity E-mail Addresses.
     * 1) Find the primary Mail Listing (a.k.a. primary e-mail) from the LDAP Listings schema.
     * 2a) If it exists, set it as the KIM Entity's default e-mail.
     * 2b) If not, then set the first LDAP Person e-mail as the default KIM Entity E-mail.
     * 3) Get all other remaining LDAP Person e-mail's and
     * compose the remainder of the list.
     */
    private List<EntityEmail> composeEmailAddressesFromLdapPerson(LdapPerson ldapPerson)
    {
        List<EntityEmail> kimEntityEmails = new ArrayList<EntityEmail>();
        String primaryEntityEmail = null;

        // Find the primary Mail Listing and set it as the KIM Entity's default e-mail
        LdapMailListing primaryMailListing = ldapMailListingService.findPrimaryMailListing(ldapPerson.getUcdPersonUUID());
        if (primaryMailListing != null)
        {
            primaryEntityEmail = primaryMailListing.getMail();
        }
        else
        {
            // No Primary Mail Listing found. Set the first LDAP Person e-mail as the KIM Entity's default e-mail.
            primaryEntityEmail = ldapPerson.getMail().get(0);
        }

        kimEntityEmails.add(composeKimEntityEmail(ldapPerson.getUcdPersonUUID(), primaryEntityEmail, EmailTypeCode.PRIMARY, true));

        // Get all other remaining LDAP Person e-mails and compose the remainder of the list.
        // Eliminate any duplicates of the primary e-mail.
        for (String mail : ldapPerson.getMail())
        {
            if (!mail.trim().equals(primaryEntityEmail.trim()))
            {
                kimEntityEmails.add(composeKimEntityEmail(ldapPerson.getUcdPersonUUID(), mail, EmailTypeCode.ADDITIONAL, false));
            }
        }
        // Fetch available e-mails from Mail Listings
        List<LdapMailListing> mailListings = ldapMailListingService.findMailListings(ldapPerson.getUcdPersonUUID());
        if (mailListings != null) {
	        for (LdapMailListing ldapMailListing : mailListings)
	        {
	            if (!ldapMailListing.getMail().equals(primaryEntityEmail))
	            {
	                kimEntityEmails.add(composeKimEntityEmail(ldapPerson.getUcdPersonUUID(), ldapMailListing.getMail(), EmailTypeCode.ADDITIONAL, false));
	            }
	        }
        }

        return kimEntityEmails;
    }

    private EntityEmail composeKimEntityEmail(String entityId, String emailAddress, EmailTypeCode emailTypeCode, boolean isDefault)
    {
        EntityEmail.Builder kimEntityEmail = EntityEmail.Builder.create(); 
        kimEntityEmail.setActive(true);
        if (isDefault)
        {
        	kimEntityEmail.setDefaultValue(true);
        }
        kimEntityEmail.setEntityId(entityId);
        kimEntityEmail.setEmailAddress(emailAddress);
        kimEntityEmail.getEmailType().setCode(emailTypeCode.name());
        return kimEntityEmail.build();
    }

    /*
     * Take an LDAP Person and compose a list of KIM Entity Phone Numbers
     */
    private List<EntityPhone> composePhoneNumbersFromLdapPerson(LdapPerson ldapPerson)
    {
        // Set Entity Phone Numbers
        if (ldapPerson.getTelephoneNumber() == null)
        {
            return null;
        }
        List<EntityPhone> kimEntityPhoneNumbers = new ArrayList<EntityPhone>(ldapPerson.getTelephoneNumber().size());
        for (String telephoneNumber : ldapPerson.getTelephoneNumber())
        {
            EntityPhone.Builder kimEntityPhone = EntityPhone.Builder.create();
            kimEntityPhone.setActive(true);
            kimEntityPhone.setEntityId(ldapPerson.getUcdPersonUUID());
            kimEntityPhone.setPhoneNumber(telephoneNumber);
            kimEntityPhoneNumbers.add(kimEntityPhone.build());
        }
        return kimEntityPhoneNumbers;
    }

    /*
     * Take an LDAP Person and compose a list of KIM Entity Names
     */
    private List<EntityName.Builder> composeNamesFromLdapPerson(LdapPerson ldapPerson) throws WhitePagesServiceException
    {
        List<EntityName.Builder> kimEntityNames = new ArrayList<EntityName.Builder>();
        WhitePagesPerson whitePagesPerson = null;

        // Get preferred name from White Pages
        if (whitePagesService != null)
        {
            try
            {
                whitePagesPerson = whitePagesService.findByPrimaryId(ldapPerson.getUcdPersonUUID());
                EntityName.Builder kimEntityPreferredName = EntityName.Builder.create();
                kimEntityPreferredName.setEntityId(ldapPerson.getUcdPersonUUID());
                kimEntityPreferredName.setId(ldapPerson.getUcdPersonUUID());
                kimEntityPreferredName.getNameType().setCode(NameTypeCode.PREFERRED.toString());
                kimEntityPreferredName.setFirstName(whitePagesPerson.getPreferredFirstName());
                kimEntityPreferredName.setMiddleName(whitePagesPerson.getPreferredMiddleName());
                kimEntityPreferredName.setLastName(whitePagesPerson.getPreferredLastName());
                kimEntityPreferredName.setActive(true);
                kimEntityPreferredName.setDefaultValue(true);
                kimEntityNames.add(kimEntityPreferredName);
            }
            // Do nothing if the Person has no entry in White Pages
            catch (EmptyResultDataAccessException e)
            {
                LOG.error("composeNamesFromLdapPerson() caught EmptyResultDataAccessException: " + e.toString());
            }
            catch (BadSqlGrammarException bsge)
            {
                throw new WhitePagesServiceException(bsge.getCause());
            }
        }

        // Get official name from LDAP
        EntityName.Builder kimEntityOfficialName = EntityName.Builder.create();
        
        kimEntityOfficialName.setEntityId(ldapPerson.getUcdPersonUUID());
        kimEntityOfficialName.setId(ldapPerson.getUcdPersonUUID());
        kimEntityOfficialName.getNameType().setCode(NameTypeCode.OFFICIAL.toString());
        /*
         * For First Name, use LDAP eduPersonNickname, if it exists. Otherwise,
         * use LDAP givenName.
         */
        if (!StringUtils.isBlank(ldapPerson.getEduPersonNickName()))
        {
            kimEntityOfficialName.setFirstName(ldapPerson.getEduPersonNickName());
        }
        else if (!StringUtils.isBlank(ldapPerson.getGivenName()))
        {
            kimEntityOfficialName.setFirstName(ldapPerson.getGivenName());
        }
        if (ldapPerson.getSn() != null)
        {
            kimEntityOfficialName.setLastName(ldapPerson.getSn().get(0));
        }
        /*
         *  If the preferred name is retrieved, set it as the default Name.
         *  Otherwise set the official name as the default name
         */
        kimEntityOfficialName.setActive(true);
        if (whitePagesPerson != null)
        {
            kimEntityOfficialName.setDefaultValue(false);
        }
        else
        {
            kimEntityOfficialName.setDefaultValue(true);
        }
        kimEntityNames.add(kimEntityOfficialName);

        return kimEntityNames;
    }

    /*
     * Take an LDAP Person and compose KIM Entity Employment Information
     */
    private List<EntityEmployment.Builder> composeEmploymentInformationFromLdapPerson(LdapPerson ldapPerson)
    {
        // Set Employment Information
        if ((ldapPerson.getDepartmentNumber() == null) && (StringUtils.isBlank(ldapPerson.getEmployeeNumber())))
        {
            return null;
        }
        List<EntityEmployment.Builder> kimEmploymentInformation = new ArrayList<EntityEmployment.Builder>();
        // Set Primary Employment Information
        EntityEmployment.Builder kimEmploymentInfo = EntityEmployment.Builder.create();
        kimEmploymentInfo.setActive(true);
        kimEmploymentInfo.setPrimary(true);
        kimEmploymentInfo.setEntityId(ldapPerson.getUcdPersonUUID());
        // Set the Employee ID if an Employee Number gets returned from LDAP
        if (!StringUtils.isBlank(ldapPerson.getEmployeeNumber()))
        {
            kimEmploymentInfo.setEmployeeId(ldapPerson.getEmployeeNumber());
        }
        kimEmploymentInfo.getEmployeeStatus().setCode("A");
        List <String> employeeType = ldapPerson.getUcdPersonAffiliation();
        if (employeeType.get(0).contains("faculty") || employeeType.get(0).contains("staff"))
        {
            kimEmploymentInfo.getEmployeeType().setCode("P");
        }
        else if (employeeType.get(0).contains("student"))
        {
            kimEmploymentInfo.getEmployeeType().setCode("N");
        }
        else
        {
            kimEmploymentInfo.getEmployeeType().setCode("O");
        }
        if (ldapPerson.getDepartmentNumber() != null)
        {
            kimEmploymentInfo.setPrimaryDepartmentCode(ldapPerson.getDepartmentNumber().get(0));
        }
        
        kimEmploymentInfo.getEmployeeStatus().setCode("A");
        kimEmploymentInformation.add(kimEmploymentInfo);
        if (LOG.isDebugEnabled())
        {
    		LOG.debug("composeEmploymentInformationFromLdapPerson() kimEmploymentInfo: " + kimEmploymentInfo.toString());
        }

        // Set Secondary Employment Information for eduPersonAffiliation
        for (String eduPersonAffiliation : ldapPerson.getEduPersonAffiliation())
        {

        	EntityEmployment.Builder kimEmploymentSecondaryInfo = EntityEmployment.Builder.create();
            kimEmploymentSecondaryInfo.setActive(true);
            // Set secondary affiliations to non-primary (false)
            kimEmploymentSecondaryInfo.setPrimary(false);
            kimEmploymentSecondaryInfo.setEntityId(ldapPerson.getUcdPersonUUID());
            kimEmploymentSecondaryInfo.getEmployeeType().setCode(eduPersonAffiliation);
            if (eduPersonAffiliation.contains("faculty") || eduPersonAffiliation.contains("staff"))
            {
                kimEmploymentSecondaryInfo.getEmployeeType().setCode("P");
            }
            else if (eduPersonAffiliation.contains("student"))
            {
                kimEmploymentSecondaryInfo.getEmployeeType().setCode("N");
            }
            else
            {
                kimEmploymentSecondaryInfo.getEmployeeType().setCode("O");
            }
            kimEmploymentSecondaryInfo.getEmployeeStatus().setCode("A");

            // Replace kimEmploymentInfo with kimEmploymentSecondaryInfo so that secondary info is added instead of primary info
            kimEmploymentInformation.add(kimEmploymentSecondaryInfo);
            if (LOG.isDebugEnabled())
            {
        		LOG.debug("composeEmploymentInformationFromLdapPerson() kimEmploymentSecondaryInfo-eduPersonAffiliation: " + kimEmploymentSecondaryInfo.toString());
            }
        }
        // Set Secondary Employment Information for ucdPersonAffiliation
        for (String ucdPersonAffiliation : ldapPerson.getUcdPersonAffiliation())
        {
            EntityEmployment.Builder kimEmploymentSecondaryInfo = EntityEmployment.Builder.create();
            kimEmploymentSecondaryInfo.setActive(false);
            // Set secondary affiliations to non-primary (false)
            kimEmploymentSecondaryInfo.setPrimary(false);
            kimEmploymentSecondaryInfo.setEntityId(ldapPerson.getUcdPersonUUID());
            kimEmploymentSecondaryInfo.getEmployeeType().setCode(ucdPersonAffiliation);
            if (ucdPersonAffiliation.contains("faculty") || ucdPersonAffiliation.contains("staff"))
            {
                kimEmploymentSecondaryInfo.getEmployeeType().setCode("P");
            }
            else if (ucdPersonAffiliation.contains("student"))
            {
                kimEmploymentSecondaryInfo.getEmployeeType().setCode("N");
            }
            else
            {
                kimEmploymentSecondaryInfo.getEmployeeType().setCode("O");
            }
            kimEmploymentSecondaryInfo.getEmployeeStatus().setCode("A");
            // Replace kimEmploymentInfo with kimEmploymentSecondaryInfo so that secondary info is added instead of primary info
            kimEmploymentInformation.add(kimEmploymentSecondaryInfo);
            if (LOG.isDebugEnabled())
            {
        		LOG.debug("composeEmploymentInformationFromLdapPerson() kimEmploymentSecondaryInfo-ucdPersonAffiliation: " + kimEmploymentSecondaryInfo.toString());
            }
        }
        return kimEmploymentInformation;
    }

    /*
     * Set KIM Entity Privacy Preferences
     */
    @Override
    public EntityPrivacyPreferences getEntityPrivacyPreferences(String entityId)
    {
        return getEntityPrivacyPreferencesImpl(entityId);
    }

    /*
     * Set KIM Entity Privacy Preferences
     */
    private EntityPrivacyPreferences.Builder getEntityPrivacyPreferencesBuilder(String entityId)
    {
    	Entity.Builder entityBuilder = Entity.Builder.create();
    	EntityPrivacyPreferences.Builder entityPrivacyPreferences = entityBuilder.getPrivacyPreferences();
    	entityPrivacyPreferences.setEntityId(entityId);
    	entityPrivacyPreferences.setSuppressAddress(false);
    	entityPrivacyPreferences.setSuppressEmail(false);
    	entityPrivacyPreferences.setSuppressName(false);
    	entityPrivacyPreferences.setSuppressPersonal(false);
    	entityPrivacyPreferences.setSuppressPhone(false);
        return entityPrivacyPreferences;
    }

    
    /*
     * Set KIM Entity Privacy Preferences
     */
    private EntityPrivacyPreferences getEntityPrivacyPreferencesImpl(String entityId)
    {
        return getEntityPrivacyPreferencesBuilder(entityId).build();
    }

    /*
     * Take in KIM search criteria and convert to UCD IdM search criteria For
     * example: - Take in: (UCDKIMEntitySearchKeys.Person.ENTITY_ID,
     * "entityValue"); - Convert to: (LdapPersonConstants.UNIVERSAL_USER_ID,
     * "entityValue");
     *
     * Throw an exception if none of the keys in the map are valid KIM Entity
     * search keys.
     *
     * @see org.kuali.rice.kim.util.KIMPropertyConstants.Person
     */
    private Map<String, String> convertSearchKeys(Map<String, String> entitySearchCriteria)
    {
        /*
         * Convert KIM Person Service search keys to UCD IdM search keys
         * Typically, the KIM Person Lookup will pass in these types of search
         * keys.
         *
         * @see org.kuali.rice.kim.service.impl.PersonServiceImpl
         */
    	if (LOG.isDebugEnabled())
        {
    		LOG.debug("convertSearchKeys() converting search keys");
        }
        Map<String, String> ucdKimEntitySearchCriteria = new HashMap<String, String>();
        for (String kimPersonServiceKey : kimPersonServiceKeyConversion.keySet())
        {
            if (entitySearchCriteria.containsKey(kimPersonServiceKey))
            {
                ucdKimEntitySearchCriteria.put(kimPersonServiceKeyConversion.get(kimPersonServiceKey), entitySearchCriteria
                        .get(kimPersonServiceKey));
            }
        }
        if (!ucdKimEntitySearchCriteria.isEmpty())
        {
            return ucdKimEntitySearchCriteria;
        }
        /*
         * Convert UCD KIM Entity search keys to UCD Idm search keys Typically,
         * those who use the KIM Identity Service API will pass in these types
         * of search keys.
         */
        Map<String, String> ucdIdmSearchCriteria = new HashMap<String, String>();
        for (String ucdKimEntityKey : ucdKimEntityKeyConversion.keySet())
        {
            if (entitySearchCriteria.containsKey(ucdKimEntityKey))
            {
                ucdIdmSearchCriteria.put(ucdKimEntityKeyConversion.get(ucdKimEntityKey), entitySearchCriteria.get(ucdKimEntityKey));
            }
        }
        if (ucdIdmSearchCriteria.isEmpty())
        {
        	if (LOG.isDebugEnabled())
            {
        		LOG.debug("Search criteria map contains no valid KIM Entity search keys.");
            }
            throw new IllegalArgumentException("Search criteria map contains no valid KIM Entity search keys.");
        }
        return ucdIdmSearchCriteria;
    }

    /*
     * Compose a KIM Entity for a system user
     */
    private Builder composeEntityForSystemUser(SystemUser user)
    {    	
        Entity.Builder kimEntityImpl = Entity.Builder.create();
        kimEntityImpl.setActive(true);
        kimEntityImpl.setId(user.getSystemUserId());

        // Set Principals
        List<Principal.Builder> kimPrincipals = new ArrayList<Principal.Builder>(1);
        Principal.Builder kimPrincipal = Principal.Builder.create(user.getSystemUserName());
        kimPrincipal.setActive(true);
        kimPrincipal.setEntityId(user.getSystemUserId());
        kimPrincipal.setPrincipalId(user.getSystemUserId());
        kimPrincipal.setPrincipalName(user.getSystemUserName());
        kimPrincipals.add(kimPrincipal);
        kimEntityImpl.setPrincipals(kimPrincipals);
        
        List<EntityTypeContactInfo.Builder> kimEntityTypes = new ArrayList<EntityTypeContactInfo.Builder>();
        EntityTypeContactInfo.Builder entityType = EntityTypeContactInfo.Builder.create(user.getSystemUserId(), "SYSTEM"); 
        entityType.setActive(true);
        entityType.setEntityTypeCode("SYSTEM");
        entityType.setEntityId(user.getSystemUserId());
        kimEntityTypes.add(entityType);
        kimEntityImpl.setEntityTypes(kimEntityTypes);

        // Set Entity Names
        List<EntityName.Builder> kimEntityNames = new ArrayList<EntityName.Builder>();
        EntityName.Builder kimEntityPreferredName = EntityName.Builder.create();
        kimEntityPreferredName.setEntityId(user.getSystemUserEntityId());
        kimEntityPreferredName.setId(user.getSystemUserName());
        kimEntityPreferredName.getNameType().setCode(NameTypeCode.PREFERRED.toString());
        kimEntityPreferredName.setFirstName(user.getSystemUserName());
        kimEntityPreferredName.setMiddleName("");
        kimEntityPreferredName.setLastName(user.getSystemUserName());
        kimEntityPreferredName.setActive(true);
        kimEntityPreferredName.setDefaultValue(true);
        kimEntityNames.add(kimEntityPreferredName);
        kimEntityImpl.setNames(kimEntityNames);

        return kimEntityImpl;
    }

//    /*
//     * Unimplement
//     */
//    @Override
//    protected Entity getEntityByKeyValue(String key, String value)
//    {
//        throw new IllegalArgumentException("getEntityByKeyValue() is not implemented.");
//    }

    /*
     * Unimplement
     */
    @Override
    public Principal getPrincipalByPrincipalNameAndPassword(String principalName, String password)
    {
        throw new IllegalArgumentException("getPrincipalByPrincipalNameAndPassword() is not implemented.");
    }
}