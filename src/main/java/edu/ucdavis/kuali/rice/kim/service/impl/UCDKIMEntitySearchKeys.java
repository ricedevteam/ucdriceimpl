/*
 * Copyright 2008-2011 The Regents of the University of California
 *
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.opensource.org/licenses/ecl2.php
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.ucdavis.kuali.rice.kim.service.impl;

// import org.kuali.rice.core.util.JSTLConstants;
import org.kuali.rice.kim.impl.KIMPropertyConstants;

/**
 * Provides search criteria keys which can be used to search against UCD
 * Identity Management.
 * 
 * @author eldavid
 * 
 */
public class UCDKIMEntitySearchKeys // extends JSTLConstants
{

    private static final long serialVersionUID = -4592510960404150218L;

    public static class Person
    {
        // TODO KR-329 defer to KIMPropertyConstants where possible
        public static final String ENTITY_ID = KIMPropertyConstants.Person.ENTITY_ID;
        public static final String PRINCIPAL_ID = KIMPropertyConstants.Person.PRINCIPAL_ID;
        public static final String PRINCIPAL_NAME = KIMPropertyConstants.Person.PRINCIPAL_NAME;
        public static final String FIRST_NAME = KIMPropertyConstants.Person.FIRST_NAME;
        public static final String LAST_NAME = KIMPropertyConstants.Person.LAST_NAME;
        public static final String EMAIL_ADDRESS = "mail";
        public static final String PHONE_NUMBER = KIMPropertyConstants.Person.PHONE_NUMBER;
        public static final String EMPLOYEE_ID = KIMPropertyConstants.Person.EMPLOYEE_ID;
        public static final String EMPLOYEE_TYPE_CODE = KIMPropertyConstants.Person.EMPLOYEE_TYPE_CODE;
        public static final String EXTERNAL_ID = KIMPropertyConstants.Person.EXTERNAL_ID;
        public static final String ADDRESS_LINE_1 = KIMPropertyConstants.Person.ADDRESS_LINE_1;
        public static final String CITY_NAME = KIMPropertyConstants.Person.CITY;
        public static final String STATE_CODE = KIMPropertyConstants.Person.STATE_CODE;
        public static final String POSTAL_CODE = KIMPropertyConstants.Person.POSTAL_CODE;
        public static final String AFFILIATION_TYPE_CODE = KIMPropertyConstants.Person.AFFILIATION_TYPE_CODE;
        public static final String PRIMARY_DEPARTMENT_CODE = KIMPropertyConstants.Person.PRIMARY_DEPARTMENT_CODE;
    }
}
