/*
 * Copyright 2008-2011 The Regents of the University of California
 *
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.opensource.org/licenses/ecl2.php
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * UC Davis implementation of the UI Document Service. This version assumes that
 * UCD Entities and related entity objects are located in the UCD LDAP Server and
 * that Kuali System Entities are in the Kuali Rice database.
 *
 * @author eldavid
 *
 */
package edu.ucdavis.kuali.rice.kim.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
//import org.kuali.rice.kim.bo.Group;
//import org.kuali.rice.kim.bo.entity.KimEntityAddress;
//import org.kuali.rice.kim.bo.entity.KimEntityAffiliation;
//import org.kuali.rice.kim.bo.entity.KimEntityEmail;
//import org.kuali.rice.kim.bo.entity.KimEntityName;
//import org.kuali.rice.kim.bo.entity.KimEntityPhone;
//import org.kuali.rice.kim.bo.entity.dto.KimEntityAffiliationInfo;
//import org.kuali.rice.kim.bo.entity.dto.KimEntityEmploymentInformationInfo;
//import org.kuali.rice.kim.bo.entity.dto.KimEntityEntityTypeInfo;
//import org.kuali.rice.kim.bo.entity.dto.KimEntityInfo;
//import org.kuali.rice.kim.bo.entity.dto.KimPrincipalInfo;
//import org.kuali.rice.kim.bo.reference.impl.EmploymentStatusImpl;
//import org.kuali.rice.kim.bo.reference.impl.EmploymentTypeImpl;

import org.kuali.rice.kim.api.group.Group;
import org.kuali.rice.kim.bo.ui.PersonDocumentAddress;
import org.kuali.rice.kim.bo.ui.PersonDocumentAffiliation;
import org.kuali.rice.kim.bo.ui.PersonDocumentEmail;
import org.kuali.rice.kim.bo.ui.PersonDocumentEmploymentInfo;
import org.kuali.rice.kim.bo.ui.PersonDocumentName;
import org.kuali.rice.kim.bo.ui.PersonDocumentPhone;
import org.kuali.rice.kim.document.IdentityManagementPersonDocument;
//import org.kuali.rice.kim.service.KIMServiceLocator;
import org.kuali.rice.kim.service.impl.UiDocumentServiceImpl;
//import org.kuali.rice.kim.util.KimConstants;
//import org.kuali.rice.kns.bo.BusinessObject;

import org.kuali.rice.kim.api.identity.principal.Principal; 
import org.kuali.rice.kim.api.identity.entity.Entity;
import org.kuali.rice.kim.api.identity.name.EntityName;
import org.kuali.rice.kim.api.identity.address.EntityAddress;
import org.kuali.rice.kim.api.identity.type.EntityTypeContactInfo;
import org.kuali.rice.kim.api.KimConstants;
import org.kuali.rice.kim.api.identity.email.EntityEmail;
import org.kuali.rice.kim.api.identity.phone.EntityPhone;
import org.kuali.rice.kim.api.services.KimApiServiceLocator;
import org.kuali.rice.kim.api.identity.affiliation.EntityAffiliation;
import org.kuali.rice.kim.api.identity.employment.EntityEmployment;
import org.kuali.rice.krad.bo.BusinessObject;
import org.kuali.rice.core.api.membership.MemberType;
import org.kuali.rice.kim.impl.identity.employment.EntityEmploymentTypeBo;
import org.kuali.rice.kim.impl.identity.employment.EntityEmploymentStatusBo;

public class UiDocumentServiceUcdImpl extends UiDocumentServiceImpl
{
    //private IdentityServiceUCDImpl identityService;
    private static final Logger LOG = Logger.getLogger(UiDocumentServiceUcdImpl.class);

    // TODO KR-364
    /**
     * 1. If the service is looking up a Principal then:
     *    a. If the Principal belongs to a System User (KR, KFS, etc.), use the Business Object Service
     *    b. Otherwise, use the Identity Management Service
     *
     * 2. For Groups and Roles, use the Business Object Service
     *
     * Note: System users will be found by the Business Object Service
     */
    /**
    *
    * @see org.kuali.rice.kim.service.UiDocumentService#loadEntityToPersonDoc(IdentityManagementPersonDocument, String)
    */
   @Override
   public void loadEntityToPersonDoc(IdentityManagementPersonDocument identityManagementPersonDocument, String principalId) {
       final Principal principal = getIdentityService().getPrincipal(principalId);

       if(principal==null)
           throw new RuntimeException("Principal does not exist for principal id:"+principalId);

       identityManagementPersonDocument.setPrincipalId(principal.getPrincipalId());
       identityManagementPersonDocument.setPrincipalName(principal.getPrincipalName());
       identityManagementPersonDocument.setActive(principal.isActive());

       Entity entity = getIdentityService().getEntityByPrincipalId(principalId);
             
       
       identityManagementPersonDocument.setEntityId(entity.getPrimaryEmployment().getEntityId());
       identityManagementPersonDocument.setAffiliations(loadAffiliations(entity.getAffiliations(),entity.getEmploymentInformation()));
       //identityManagementPersonDocument.setNames(loadName(kimEntity.getDefaultName()));
       identityManagementPersonDocument.setNames(loadNames(entity.getNames()));

       EntityTypeContactInfo entityType = null;
       for (EntityTypeContactInfo type : entity.getEntityTypeContactInfos()) {
           if (KimConstants.EntityTypes.PERSON.equals(type.getEntityTypeCode())) {
               entityType = type;
           }
       }

       if (entityType != null)
       {
           identityManagementPersonDocument.setEmails(loadEmails(entityType.getEmailAddresses()));
           identityManagementPersonDocument.setPhones(loadPhones(entityType.getPhoneNumbers()));
           identityManagementPersonDocument.setAddrs(loadAddresses(entityType.getAddresses()));
       }

       List<? extends Group> groups = getGroupService().getGroupsByPrincipalId(identityManagementPersonDocument.getPrincipalId());
       loadGroupToPersonDoc(identityManagementPersonDocument, groups);
       loadRoleToPersonDoc(identityManagementPersonDocument);
       loadDelegationsToPersonDoc(identityManagementPersonDocument);
   }

protected List<PersonDocumentAffiliation> loadAffiliations(List <? extends EntityAffiliation> affiliations, EntityEmployment empInfo) {
       List<PersonDocumentAffiliation> docAffiliations = new ArrayList<PersonDocumentAffiliation>();
       for (EntityAffiliation affiliation: affiliations) {
           if(affiliation.isActive()){
               PersonDocumentAffiliation docAffiliation = new PersonDocumentAffiliation();
               docAffiliation.setAffiliationTypeCode(affiliation.getAffiliationType().getCode());
               docAffiliation.setCampusCode(affiliation.getCampusCode());
               docAffiliation.setActive(affiliation.isActive());
               docAffiliation.setDflt(affiliation.isDefaultValue());
               docAffiliation.setEntityAffiliationId(affiliation.getEntityId());
               docAffiliation.refreshReferenceObject("affiliationType");
               docAffiliations.add(docAffiliation);
               docAffiliation.setEdit(true);

               List<PersonDocumentEmploymentInfo> docEmploymentInformations = new ArrayList<PersonDocumentEmploymentInfo>();
               if (empInfo != null && empInfo.isActive() && docAffiliation.getEntityAffiliationId().equals(empInfo.getEntityAffiliation().getEntityId()))
               {
                   PersonDocumentEmploymentInfo docEmpInfo = new PersonDocumentEmploymentInfo();
                   docEmpInfo.setEntityEmploymentId(empInfo.getEntityId());
                   docEmpInfo.setEmployeeId(empInfo.getEmployeeId());
                   docEmpInfo.setEmploymentRecordId(empInfo.getEmploymentRecordId());
                   docEmpInfo.setBaseSalaryAmount(empInfo.getBaseSalaryAmount());
                   docEmpInfo.setPrimaryDepartmentCode(empInfo.getPrimaryDepartmentCode());
                   docEmpInfo.setEmploymentTypeCode(empInfo.getEmployeeType().getCode()==""?"O":empInfo.getEmployeeType().getCode());
                   docEmpInfo.setEmploymentStatusCode(empInfo.getEmployeeStatus().getCode()==""?"A":empInfo.getEmployeeStatus().getCode());
                   docEmpInfo.setActive(empInfo.isActive());
                   docEmpInfo.setPrimary(empInfo.isPrimary());
                   docEmpInfo.setEntityAffiliationId(empInfo.getEntityAffiliation().getEntityId());
                   docEmpInfo.setEdit(true);
                   docEmpInfo.setVersionNumber(0L);
                   
                   EntityEmploymentTypeBo empTypeImpl = new EntityEmploymentTypeBo();
                   empTypeImpl.setCode(docEmpInfo.getEmploymentTypeCode());
                   EntityEmploymentStatusBo empStatusImpl = new EntityEmploymentStatusBo();
                   empStatusImpl.setCode(docEmpInfo.getEmploymentStatusCode());
                   docEmpInfo.setEmploymentType(empTypeImpl);
                   docEmpInfo.setEmploymentStatus(empStatusImpl);
                   docEmpInfo.refreshReferenceObject("employmentType");
                   docEmpInfo.refreshReferenceObject("employmentStatus");
                   docEmploymentInformations.add(docEmpInfo);
               }
               docAffiliation.setEmpInfos(docEmploymentInformations);
           }
       }
       return docAffiliations;

   }


   protected List<PersonDocumentName> loadNames(List <? extends EntityName> names) {
       List<PersonDocumentName> docNames = new ArrayList<PersonDocumentName>();
       for (EntityName name: names) {
           if(name != null && name.isActive()){
               PersonDocumentName docName = new PersonDocumentName();
               docName.setNameCode(name.getNameType().getCode());
               docName.setFirstName(name.getFirstName());
               docName.setLastName(name.getLastName());
               docName.setMiddleName(name.getMiddleName());
               docName.setNameTitle(name.getNameTitle());
               docName.setNameSuffix(name.getNameSuffix());
               docName.setActive(name.isActive());
               docName.setDflt(name.isDefaultValue());
               docName.setEdit(true);
               docNames.add(docName);
           }
       }
       return docNames;
   }

   protected List<PersonDocumentAddress> loadAddresses(List <? extends EntityAddress>  addresses) {
       List<PersonDocumentAddress> docAddresses = new ArrayList<PersonDocumentAddress>();

       for (EntityAddress address: addresses)
       {
           if(address != null && address.isActive()){
               PersonDocumentAddress docAddress = new PersonDocumentAddress();
               docAddress.setEntityTypeCode(address.getEntityTypeCode());
               docAddress.setAddressTypeCode(address.getAddressType().getCode());
               docAddress.setLine1(address.getLine1());
               docAddress.setLine2(address.getLine2());
               docAddress.setLine3(address.getLine3());
               docAddress.setStateProvinceCode(address.getStateProvinceCode());
               docAddress.setPostalCode(address.getPostalCode());
               docAddress.setCountryCode(address.getCountryCode());
               docAddress.setCity(address.getCity());
               docAddress.setActive(address.isActive());
               docAddress.setDflt(address.isDefaultValue());
               docAddress.setEdit(true);
               docAddresses.add(docAddress);
           }
       }
       return docAddresses;
   }

   protected List<PersonDocumentEmail> loadEmails(List <? extends EntityEmail> emaillist) {
       List<PersonDocumentEmail> emails = new ArrayList<PersonDocumentEmail>();

       for (EntityEmail email : emaillist)
       {
           if(email != null && email.isActive()){
               PersonDocumentEmail docEmail = new PersonDocumentEmail();
               //docEmail.setEntityId(email.getEntityId());
               docEmail.setEntityTypeCode(email.getEntityTypeCode());
               docEmail.setEmailTypeCode(email.getEmailType().getCode());
               docEmail.setEmailAddress(email.getEmailAddress());
               docEmail.setActive(email.isActive());
               docEmail.setDflt(email.isDefaultValue());
               docEmail.setEdit(true);
               emails.add(docEmail);
           }
       }
       return emails;
   }

   protected List<PersonDocumentPhone> loadPhones(List <? extends EntityPhone> phones) {
       List<PersonDocumentPhone> docPhones = new ArrayList<PersonDocumentPhone>();

       for (EntityPhone phone: phones)
       {
           if(phone != null && phone.isActive()){
               PersonDocumentPhone docPhone = new PersonDocumentPhone();
               docPhone.setPhoneTypeCode(phone.getPhoneType().getCode());
               docPhone.setEntityTypeCode(phone.getEntityTypeCode());
               docPhone.setPhoneNumber(phone.getPhoneNumber());
               docPhone.setCountryCode(phone.getCountryCode());
               docPhone.setExtensionNumber(phone.getExtensionNumber());
               docPhone.setActive(phone.isActive());
               docPhone.setDflt(phone.isDefaultValue());
               docPhone.setEdit(true);
               docPhones.add(docPhone);
           }
       }
       return docPhones;

   }

    @Override
    public String getMemberName(MemberType memberType, String memberId){
        if(memberType == null || StringUtils.isEmpty(memberType.getCode()) || StringUtils.isEmpty(memberId))
        {
        	return "";
        }
        BusinessObject member = getMember(memberType, memberId);
        LOG.debug("getMemberName() Fetching to get Principal Name for ID == "+ memberId +" for Member Type Code == " + memberType.getCode());
        if (member != null)
        {
            // If we're here, that means the member is either a System User, Group, or Role
            return getMemberName(memberType, member);
        }
        else
        {
            // If we're here, that means the member is not in the Rice DB.
        	LOG.debug("getMemberName() Member ID " + memberId + " not found in the Rice DB. Trying Identity Management Service.");
            return getMemberName(memberId);
        }
    }

    /*
     * Find a Member Name using the Identity Management Service
     */
    private String getMemberName(String memberId)
    {
    	// KR-607: account for roleMemberName being null
        Principal principal = null;
        principal = KimApiServiceLocator.getIdentityService().getPrincipal(memberId);
        if(principal == null) {
        	LOG.debug("getMemberName() Member ID " + memberId + " not found via Identity Management Service.");
			return "";
		}
        return principal.getPrincipalName();
    }
}
