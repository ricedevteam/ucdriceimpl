/*
 * Copyright 2008-2011 The Regents of the University of California
 *
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.opensource.org/licenses/ecl2.php
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * UC Davis implementation of the Workflow Info Service. This version assumes that
 * UCD Entities and related entity objects are located in the UCD LDAP Server and
 * that Kuali System Entities are in the Kuali Rice database.
 * 
 * @author eldavid
 * 
 */package edu.ucdavis.kuali.rice.kns.workflow.service.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

//import org.kuali.rice.kew.dto.RouteHeaderDTO;
//import org.kuali.rice.kew.exception.WorkflowException;
//import org.kuali.rice.kew.service.WorkflowInfo;
//import org.kuali.rice.kim.bo.entity.impl.KimPrincipalImpl;
//import org.kuali.rice.kim.util.KIMPropertyConstants;
//import org.kuali.rice.kns.service.BusinessObjectService;
//import org.kuali.rice.kns.service.KNSServiceLocator;
//import org.kuali.rice.kns.util.KNSConstants;
//import org.kuali.rice.kns.workflow.service.impl.KualiWorkflowInfoImpl;

public class KualiWorkflowInfoUcdImpl //extends KualiWorkflowInfoImpl
{
//    //TODO KR-439    
//    private BusinessObjectService businessObjectService;
//    
//    private WorkflowInfo workflowInfo;
//    
//    private WorkflowInfo getWorkflowUtility() {
//        return workflowInfo;
//    }
//
//    protected BusinessObjectService getBusinessObjectService() {
//        if ( businessObjectService == null ) {
//                businessObjectService = KNSServiceLocator.getBusinessObjectService();
//        }
//        return businessObjectService;
//    }
//    
//    /** 
//     *  Get the System User (KR), by using the Business Object Service instead of the Identity Management Service
//     *  Borrowed from org.kuali.rice.kim.service.IdentityService#getPrincipalByPrincipalName(java.lang.String)
//     */ 
//    @SuppressWarnings("unchecked")
//    @Override
//    public RouteHeaderDTO getRouteHeader(Long routeHeaderId) throws WorkflowException {
//        //KimPrincipal principal = KIMServiceLocator.getIdentityManagementService().getPrincipalByPrincipalName(KNSConstants.SYSTEM_USER);
//        Map<String,Object> criteria = new HashMap<String,Object>(1);
//        criteria.put(KIMPropertyConstants.Principal.PRINCIPAL_NAME, KNSConstants.SYSTEM_USER.toLowerCase());
//        Collection<KimPrincipalImpl> principals = (Collection<KimPrincipalImpl>)getBusinessObjectService().findMatching(KimPrincipalImpl.class, criteria);
//        if (!principals.isEmpty() && principals.size() == 1) {
//            return getWorkflowUtility().getRouteHeader(principals.iterator().next().getPrincipalId(), routeHeaderId);
//        }
//        else
//        {
//            throw new WorkflowException("Failed to locate System User with principal name 'kr'");
//        }
//    }
}
