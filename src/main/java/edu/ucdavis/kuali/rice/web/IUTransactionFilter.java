/*
 * Copyright 2008-2011 The Regents of the University of California
 *
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.opensource.org/licenses/ecl2.php
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * A filter written by Indiana University that detects when "closed connection" exceptions (See KR-598) 
 * are likely going to be showing up in the same thread (it would be a TP-Processor thread in Tomcat. 
 * The closed connection exceptions start to happen a few hours hours after the actual event that 
 * triggered the problem.
 * 
 * @author eldavid
 * 
 */

package edu.ucdavis.kuali.rice.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.kuali.rice.core.api.CoreApiServiceLocator;
import org.kuali.rice.core.api.mail.EmailBody;
import org.kuali.rice.core.api.mail.EmailFrom;
import org.kuali.rice.core.api.mail.EmailSubject;
import org.kuali.rice.core.api.mail.EmailToList;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.transaction.support.TransactionSynchronizationManager;

public class IUTransactionFilter implements Filter {
    private static final Logger LOG = Logger.getLogger(IUTransactionFilter.class);
 
    public void doFilter(ServletRequest req, ServletResponse res,	           
    		FilterChain chain) throws IOException, ServletException {
    	
    	boolean error = false;
    	String errorText = "";
    	HttpServletRequest httpReq = (HttpServletRequest)req;
    			
		LOG.debug("At Start -- URL: " + httpReq.getRequestURL() + "  ;");
						    	
    	if (!TransactionSynchronizationManager.getResourceMap().isEmpty()){
    		errorText = errorText + ("Before: The Resource map is not empty.   THIS IS SOOO BAD.   URL: " + httpReq.getRequestURL());
    		LOG.error("Before: The Resource map is not empty.   THIS IS SOOO BAD.   URL: " + httpReq.getRequestURL());
    		
    		Map aMap = TransactionSynchronizationManager.getResourceMap();
    		int mapsize = aMap.size();

    		Iterator keyValuePairs1 = aMap.entrySet().iterator();
    		for (int i = 0; i < mapsize; i++)
    		{
    		  Map.Entry entry = (Map.Entry) keyValuePairs1.next();
    		  errorText = errorText + (";  Resources:  key: " + entry.getKey().toString() + " ; Value " + entry.getValue().toString());
    		  LOG.error("Resources:  key: " + entry.getKey().toString() + " ; Value " + entry.getValue().toString());
    		}
    		error = true;
    	} else {
    		LOG.debug("Before: The Resource map is empty.  This is good.    URL: " + httpReq.getRequestURL());
    	}
    	    	
		try {
			chain.doFilter(req, res);
		} finally { 
	
			LOG.debug("At End ---- URL: " + httpReq.getRequestURL() + "  ;");
			
			if (TransactionSynchronizationManager.isSynchronizationActive()){
				LOG.error("JTA synchronizations are not active.   THIS IS SOOO BAD.   " + httpReq.getRequestURL());
				error = true;
			}
    	
			if (!TransactionSynchronizationManager.getResourceMap().isEmpty()){
				errorText = errorText + ("After: The Resource map is not empty.   THIS IS SOOO BAD.");
				LOG.error("After: The Resource map is not empty.   THIS IS SOOO BAD.   URL: " + httpReq.getRequestURL());
    		
				Map aMap = TransactionSynchronizationManager.getResourceMap();
				int mapsize = aMap.size();

				Iterator keyValuePairs1 = aMap.entrySet().iterator();
				for (int i = 0; i < mapsize; i++)
				{
					Map.Entry entry = (Map.Entry) keyValuePairs1.next();
					errorText = errorText + (";  Resources:  key: " + entry.getKey().toString() + " ; Value " + entry.getValue().toString());
					LOG.error("Resources:  key: " + entry.getKey().toString() + " ; Value " + entry.getValue().toString());
				}
				error = true;
			} else {
				LOG.debug("After: The Resource map is empty.  This is good.    URL: " + httpReq.getRequestURL());
			}
    	
			if (error) {
				try {
					LOG.error("ERROR Found!  Sending an email...");
					
					List<String> toAddresses  = new ArrayList<String>();
				    toAddresses.add("rice-admin@ucdavis.edu");

					SimpleMailMessage smm = new SimpleMailMessage();
					smm.setTo("rice-admin@ucdavis.edu");
					smm.setSubject("There might be a closed connection problem.  Please check the logs. Seach for the following phrase: SOOO BAD");
					smm.setText("Request URL which had the problem: " + httpReq.getRequestURL() + "    errorText = " + errorText);
					smm.setFrom("rice-admin@ucdavis.edu");
					
					CoreApiServiceLocator.getMailer().sendEmail(
							new EmailFrom(smm.getFrom()), 
							new EmailToList(toAddresses), 
							new EmailSubject(smm.getSubject()), 
							new EmailBody(smm.getText()),
							null,
							null,
							false);
			
				} catch (Throwable t) {
					LOG.error("Error sending email.", t);
				}
			}
		}
    }

	public void destroy() {		
	}

	public void init(FilterConfig arg0) throws ServletException {
	}
}
