/*
 * Copyright 2008-2011 The Regents of the University of California
 *
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.opensource.org/licenses/ecl2.php
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.ucdavis.kuali.rice.kim.service.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.apache.commons.dbcp.BasicDataSource;
import org.dbunit.DatabaseTestCase;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.kuali.rice.core.api.criteria.QueryByCriteria;
import org.kuali.rice.core.api.criteria.QueryByCriteria.Builder;
import org.kuali.rice.kew.identity.service.IdentityHelperService;
import org.kuali.rice.kim.api.identity.IdentityService;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.kuali.rice.kim.api.identity.entity.EntityDefault;
import org.kuali.rice.kim.api.identity.principal.Principal;
import org.kuali.rice.kim.api.identity.entity.Entity;
import org.kuali.rice.kim.api.identity.type.EntityTypeContactInfo;
import org.kuali.rice.kim.api.identity.email.EntityEmail;
import org.kuali.rice.kim.api.identity.affiliation.EntityAffiliation;

import edu.ucdavis.kuali.rice.kim.domain.SystemUser;
import edu.ucdavis.kuali.rice.kim.service.impl.UCDKIMEntitySearchKeys;

public class IdentityArchiveServiceTest extends DatabaseTestCase
{
    private String[] springFiles = {"classpath:edu/ucdavis/kuali/rice/test/config/test-common.xml"
                                   ,"classpath:edu/ucdavis/kuali/rice/kim/config/ldap-data.xml"
                                   ,"classpath:edu/ucdavis/kuali/rice/test/config/rice-service.xml"
                                   ,"classpath:edu/ucdavis/kuali/rice/kim/config/ucd-kim-service.xml"};

    private ApplicationContext context = new ClassPathXmlApplicationContext(springFiles);
    //private final IdentityArchiveService identityArchiveService = (IdentityArchiveService) context.getBean("kimIdentityArchiveService");
    private final IdentityService identityService = (IdentityService)context.getBean("kimIdentityService");
    private final IdentityHelperService identityHelperService = (IdentityHelperService)context.getBean("kewIdentityHelperService");
    private final BasicDataSource dataSource = (BasicDataSource)context.getBean("kimNonTransactionalDataSource");
    private final String entityId = "9999999996119312177817";
    private final String principalId = "LastSonOfKrypton";
    private final String principalName = "superman";
    private final SystemUser kruser = (SystemUser)context.getBean("kruser");
    private final SystemUser kfsuser = (SystemUser)context.getBean("kfsuser");
    private final SystemUser adminuser = (SystemUser)context.getBean("adminuser");
    private final SystemUser mivuser = (SystemUser)context.getBean("mivuser");
    
    protected void setUp() throws Exception
    {
        super.setUp();
    }

    @SuppressWarnings("unchecked")
    @Override
    protected IDatabaseConnection getConnection() throws Exception
    {
        // TODO KR-458
        @SuppressWarnings("unused")
        Class driverClass = Class.forName(dataSource.getDriverClassName());
        Connection jdbcConnection = DriverManager.getConnection(dataSource.getUrl(), dataSource.getUsername(), dataSource.getPassword());
        return new DatabaseConnection(jdbcConnection);
    }

    @Override
    protected IDataSet getDataSet() throws Exception
    {
        // TODO KR-458
        FlatXmlDataSet loadedDataSet = new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("KRIM_ENTITY_CACHE_T.xml"));
        return loadedDataSet;
    }
    
    /*
     * Test to see that the data was loaded into the test database
     */
    public void testCheckDataLoaded() throws Exception
    {
        IDataSet loadedDataSet = getDataSet();
        assertNotNull(loadedDataSet);
        int rowCount = loadedDataSet.getTable("KRIM_ENTITY_CACHE_T").getRowCount();
        assertEquals(1, rowCount);
    }

    /*
     * Find an Entity from KRIM_ENTITY_CACHE_T by Entity Id
     */
    public void testGetEntityDefaultInfo()
    {
        EntityDefault entityDefaultInfo = identityService.getEntityDefault(entityId);
        assertEquals(entityId, entityDefaultInfo.getEntityId());
        System.out.println("Entity ID: " + entityDefaultInfo.getEntityId());
    }

    /*
     * Find an Entity from KRIM_ENTITY_CACHE_T Principal ID
     */
    public void testGetEntityDefaultInfoByPrincipalId()
    {
        EntityDefault entityDefaultInfo = identityService.getEntityDefaultByPrincipalId(principalId);
        assertEquals(entityId, entityDefaultInfo.getEntityId());
        for (Principal kimPrincipalInfo : entityDefaultInfo.getPrincipals())
        {
            System.out.println("Principal ID: " + kimPrincipalInfo.getPrincipalId());
            assertEquals(principalId, kimPrincipalInfo.getPrincipalId());
        }
    }
    
    /*
     * Find an Entity from KRIM_ENTITY_CACHE_T by Principal Name
     */
    public void testGetEntityDefaultInfoByPrincipalName()
    {
        
        EntityDefault entityDefaultInfo = identityService.getEntityDefaultByPrincipalName(principalName);
        assertEquals(entityId, entityDefaultInfo.getEntityId());
        for (Principal kimPrincipalInfo : entityDefaultInfo.getPrincipals())
        {
            assertEquals(principalName, kimPrincipalInfo.getPrincipalName());
            System.out.println("Principal Name: " + kimPrincipalInfo.getPrincipalName());
        }
    }
 
    // TODO KR-529
    /*
     * Find a Principal from cache by Principal ID using Identity Helper Service
     */
    public void testGetPrincipalByPrincipalId()
    {
        Principal principalInfo = identityService.getPrincipal(principalId);
        System.out.println("Principal ID: " + principalInfo.getPrincipalId());
        assertEquals(entityId, principalInfo.getEntityId());        
        assertEquals(principalId, principalInfo.getPrincipalId());
        assertEquals(principalName, principalInfo.getPrincipalName());
    }
    
    // TODO KR-508
    /*
     * Find a System User's Default Info by Entity Id
     */
    public void testGetSystemUserEntityDefaultInfo()
    {
        // Find the KR System User
        EntityDefault krSystemUser = identityService.getEntityDefault("1");
        Assert.assertEquals(krSystemUser.getEntityId(), kruser.getSystemUserEntityId());  
        Assert.assertEquals(krSystemUser.getPrincipals().get(0).getPrincipalId(), kruser.getSystemUserId());
        Assert.assertEquals(krSystemUser.getPrincipals().get(0).getPrincipalName(), kruser.getSystemUserName());
        // Find the KFS System User
        EntityDefault kfsSystemUser = identityService.getEntityDefault("2");
        Assert.assertEquals(kfsSystemUser.getEntityId(), kfsuser.getSystemUserEntityId());  
        Assert.assertEquals(kfsSystemUser.getPrincipals().get(0).getPrincipalId(), kfsuser.getSystemUserId());
        Assert.assertEquals(kfsSystemUser.getPrincipals().get(0).getPrincipalName(), kfsuser.getSystemUserName());
        // Find the Admin System User
        EntityDefault adminSystemUser = identityService.getEntityDefault("1100");
        Assert.assertEquals(adminSystemUser.getEntityId(), adminuser.getSystemUserEntityId());  
        Assert.assertEquals(adminSystemUser.getPrincipals().get(0).getPrincipalId(), adminuser.getSystemUserId());
        Assert.assertEquals(adminSystemUser.getPrincipals().get(0).getPrincipalName(), adminuser.getSystemUserName());        
        // Find the MIV System User
        EntityDefault mivSystemUser = identityService.getEntityDefault("9001");
        Assert.assertEquals(mivSystemUser.getEntityId(), mivuser.getSystemUserEntityId());  
        Assert.assertEquals(mivSystemUser.getPrincipals().get(0).getPrincipalId(), mivuser.getSystemUserId());
        Assert.assertEquals(mivSystemUser.getPrincipals().get(0).getPrincipalName(), mivuser.getSystemUserName());
    }

    // TODO KR-508
    /*
     * Find a System User's Default Info by Principal ID
     */
    public void testGetSystemUserEntityDefaultInfoByPrincipalId()
    {
        // Find the KR System User
    	EntityDefault krSystemUser = identityService.getEntityDefaultByPrincipalId("1");
        Assert.assertEquals(krSystemUser.getEntityId(), kruser.getSystemUserEntityId());  
        Assert.assertEquals(krSystemUser.getPrincipals().get(0).getPrincipalId(), kruser.getSystemUserId());
        Assert.assertEquals(krSystemUser.getPrincipals().get(0).getPrincipalName(), kruser.getSystemUserName());
        // Find the KFS System User
        EntityDefault kfsSystemUser = identityService.getEntityDefaultByPrincipalId("2");
        Assert.assertEquals(kfsSystemUser.getEntityId(), kfsuser.getSystemUserEntityId());  
        Assert.assertEquals(kfsSystemUser.getPrincipals().get(0).getPrincipalId(), kfsuser.getSystemUserId());
        Assert.assertEquals(kfsSystemUser.getPrincipals().get(0).getPrincipalName(), kfsuser.getSystemUserName());
        // Find the Admin System User
        EntityDefault adminSystemUser = identityService.getEntityDefaultByPrincipalId("1100");
        Assert.assertEquals(adminSystemUser.getEntityId(), adminuser.getSystemUserEntityId());  
        Assert.assertEquals(adminSystemUser.getPrincipals().get(0).getPrincipalId(), adminuser.getSystemUserId());
        Assert.assertEquals(adminSystemUser.getPrincipals().get(0).getPrincipalName(), adminuser.getSystemUserName());         
        // Find the MIV System User
        EntityDefault mivSystemUser = identityService.getEntityDefaultByPrincipalId("9001");
        Assert.assertEquals(mivSystemUser.getEntityId(), mivuser.getSystemUserEntityId());  
        Assert.assertEquals(mivSystemUser.getPrincipals().get(0).getPrincipalId(), mivuser.getSystemUserId());
        Assert.assertEquals(mivSystemUser.getPrincipals().get(0).getPrincipalName(), mivuser.getSystemUserName());
    }

    // TODO KR-508
    /*
     * Find a System User's Default Info by Principal Name
     */
    public void testGetSystemUserEntityDefaultInfoByPrincipalName()
    {
        // Find the KR System User
    	EntityDefault krSystemUser = identityService.getEntityDefaultByPrincipalName("kr");
        Assert.assertEquals(krSystemUser.getEntityId(), kruser.getSystemUserEntityId());  
        Assert.assertEquals(krSystemUser.getPrincipals().get(0).getPrincipalId(), kruser.getSystemUserId());
        Assert.assertEquals(krSystemUser.getPrincipals().get(0).getPrincipalName(), kruser.getSystemUserName());
        // Find the KFS System User
        EntityDefault kfsSystemUser = identityService.getEntityDefaultByPrincipalName("kfs");
        Assert.assertEquals(kfsSystemUser.getEntityId(), kfsuser.getSystemUserEntityId());  
        Assert.assertEquals(kfsSystemUser.getPrincipals().get(0).getPrincipalId(), kfsuser.getSystemUserId());
        Assert.assertEquals(kfsSystemUser.getPrincipals().get(0).getPrincipalName(), kfsuser.getSystemUserName());        
        // Find the Admin System User
        EntityDefault adminSystemUser = identityService.getEntityDefaultByPrincipalName("admin");
        Assert.assertEquals(adminSystemUser.getEntityId(), adminuser.getSystemUserEntityId());  
        Assert.assertEquals(adminSystemUser.getPrincipals().get(0).getPrincipalId(), adminuser.getSystemUserId());
        Assert.assertEquals(adminSystemUser.getPrincipals().get(0).getPrincipalName(), adminuser.getSystemUserName()); 
        // Find the MIV System User
        EntityDefault mivSystemUser = identityService.getEntityDefaultByPrincipalName("MIV-SYSTEM");
        Assert.assertEquals(mivSystemUser.getEntityId(), mivuser.getSystemUserEntityId());  
        Assert.assertEquals(mivSystemUser.getPrincipals().get(0).getPrincipalId(), mivuser.getSystemUserId());
        Assert.assertEquals(mivSystemUser.getPrincipals().get(0).getPrincipalName(), mivuser.getSystemUserName());
    }

    // TODO KR-508
    /*
     * Find a System User by Entity ID 
     * Test: protected List<KimEntityImpl> lookupEntitys(Map<String, String> searchCriteria)
     */
//    public void testLookupSystemUserEntityByEntityId()
//    {
//        // Find the KR System User
//    	Map<String,String> krSearchCriteria = new HashMap<String,String>(1);
//        krSearchCriteria.put(UCDKIMEntitySearchKeys.Person.ENTITY_ID, "1");
//        List<EntityDefault> krSystemUserDefaultInfo = identityService.lookupEntityDefaultInfo(krSearchCriteria, true);
//        Assert.assertEquals(1, krSystemUserDefaultInfo.size());
//        Assert.assertEquals(krSystemUserDefaultInfo.get(0).getEntityId(), kruser.getSystemUserEntityId());  
//        Assert.assertEquals(krSystemUserDefaultInfo.get(0).getPrincipals().get(0).getPrincipalId(), kruser.getSystemUserId());
//        Assert.assertEquals(krSystemUserDefaultInfo.get(0).getPrincipals().get(0).getPrincipalName(), kruser.getSystemUserName());
//        // Find the KFS System User
//        Map<String,String> kfsSearchCriteria = new HashMap<String,String>(1);
//        kfsSearchCriteria.put(UCDKIMEntitySearchKeys.Person.ENTITY_ID, "2");
//        List<EntityDefault> kfsSystemUserDefaultInfo = identityService.lookupEntityDefaultInfo(kfsSearchCriteria, true);
//        Assert.assertEquals(1, kfsSystemUserDefaultInfo.size());
//        Assert.assertEquals(kfsSystemUserDefaultInfo.get(0).getEntityId(), kfsuser.getSystemUserEntityId());  
//        Assert.assertEquals(kfsSystemUserDefaultInfo.get(0).getPrincipals().get(0).getPrincipalId(), kfsuser.getSystemUserId());
//        Assert.assertEquals(kfsSystemUserDefaultInfo.get(0).getPrincipals().get(0).getPrincipalName(), kfsuser.getSystemUserName());        
//        // Find the Admin System User
//        Map<String,String> adminSearchCriteria = new HashMap<String,String>(1);
//        adminSearchCriteria.put(UCDKIMEntitySearchKeys.Person.ENTITY_ID, "1100");
//        List<EntityDefault> adminSystemUserDefaultInfo = identityService.lookupEntityDefaultInfo(adminSearchCriteria, true);
//        Assert.assertEquals(1, adminSystemUserDefaultInfo.size());
//        Assert.assertEquals(adminSystemUserDefaultInfo.get(0).getEntityId(), adminuser.getSystemUserEntityId());  
//        Assert.assertEquals(adminSystemUserDefaultInfo.get(0).getPrincipals().get(0).getPrincipalId(), adminuser.getSystemUserId());
//        Assert.assertEquals(adminSystemUserDefaultInfo.get(0).getPrincipals().get(0).getPrincipalName(), adminuser.getSystemUserName()); 
//        // Find the MIV System User
//        Map<String,String> mivSearchCriteria = new HashMap<String,String>(1);
//        mivSearchCriteria.put(UCDKIMEntitySearchKeys.Person.ENTITY_ID, "9001");
//        List<EntityDefault> mivSystemUserDefaultInfo = identityService.lookupEntityDefaultInfo(mivSearchCriteria, true);
//        Assert.assertEquals(1, mivSystemUserDefaultInfo.size());
//        Assert.assertEquals(mivSystemUserDefaultInfo.get(0).getEntityId(), mivuser.getSystemUserEntityId());  
//        Assert.assertEquals(mivSystemUserDefaultInfo.get(0).getPrincipals().get(0).getPrincipalId(), mivuser.getSystemUserId());
//        Assert.assertEquals(mivSystemUserDefaultInfo.get(0).getPrincipals().get(0).getPrincipalName(), mivuser.getSystemUserName());
//    }    
    
    // TODO KR-508
    /*
     * Find a System User by Principal ID 
     * Test: protected List<KimEntityImpl> lookupEntitys(Map<String, String> searchCriteria)
     */
//    public void testLookupSystemUserEntityByPrincipalId()
//    {
//        // Find the KR System User
//        Map<String,String> krSearchCriteria = new HashMap<String,String>(1);
//        krSearchCriteria.put(UCDKIMEntitySearchKeys.Person.PRINCIPAL_ID, "1");
//        List<EntityDefault> krSystemUserDefaultInfo = identityService.lookupEntityDefaultInfo(krSearchCriteria, true);
//        Assert.assertEquals(1, krSystemUserDefaultInfo.size());
//        Assert.assertEquals(krSystemUserDefaultInfo.get(0).getEntityId(), kruser.getSystemUserEntityId());  
//        Assert.assertEquals(krSystemUserDefaultInfo.get(0).getPrincipals().get(0).getPrincipalId(), kruser.getSystemUserId());
//        Assert.assertEquals(krSystemUserDefaultInfo.get(0).getPrincipals().get(0).getPrincipalName(), kruser.getSystemUserName());
//        // Find the KFS System User
//        Map<String,String> kfsSearchCriteria = new HashMap<String,String>(1);
//        kfsSearchCriteria.put(UCDKIMEntitySearchKeys.Person.PRINCIPAL_ID, "2");
//        List<EntityDefault> kfsSystemUserDefaultInfo = identityService.lookupEntityDefaultInfo(kfsSearchCriteria, true);
//        Assert.assertEquals(1, kfsSystemUserDefaultInfo.size());
//        Assert.assertEquals(kfsSystemUserDefaultInfo.get(0).getEntityId(), kfsuser.getSystemUserEntityId());  
//        Assert.assertEquals(kfsSystemUserDefaultInfo.get(0).getPrincipals().get(0).getPrincipalId(), kfsuser.getSystemUserId());
//        Assert.assertEquals(kfsSystemUserDefaultInfo.get(0).getPrincipals().get(0).getPrincipalName(), kfsuser.getSystemUserName());        
//        // Find the Admin System User
//        Map<String,String> adminSearchCriteria = new HashMap<String,String>(1);
//        adminSearchCriteria.put(UCDKIMEntitySearchKeys.Person.PRINCIPAL_ID, "1100");
//        List<EntityDefault> adminSystemUserDefaultInfo = identityService.lookupEntityDefaultInfo(adminSearchCriteria, true);
//        Assert.assertEquals(1, adminSystemUserDefaultInfo.size());
//        Assert.assertEquals(adminSystemUserDefaultInfo.get(0).getEntityId(), adminuser.getSystemUserEntityId());  
//        Assert.assertEquals(adminSystemUserDefaultInfo.get(0).getPrincipals().get(0).getPrincipalId(), adminuser.getSystemUserId());
//        Assert.assertEquals(adminSystemUserDefaultInfo.get(0).getPrincipals().get(0).getPrincipalName(), adminuser.getSystemUserName()); 
//        // Find the MIV System User
//        Map<String,String> mivSearchCriteria = new HashMap<String,String>(1);
//        mivSearchCriteria.put(UCDKIMEntitySearchKeys.Person.PRINCIPAL_ID, "9001");
//        List<EntityDefault> mivSystemUserDefaultInfo = identityService.lookupEntityDefaultInfo(mivSearchCriteria, true);
//        Assert.assertEquals(1, mivSystemUserDefaultInfo.size());
//        Assert.assertEquals(mivSystemUserDefaultInfo.get(0).getEntityId(), mivuser.getSystemUserEntityId());  
//        Assert.assertEquals(mivSystemUserDefaultInfo.get(0).getPrincipals().get(0).getPrincipalId(), mivuser.getSystemUserId());
//        Assert.assertEquals(mivSystemUserDefaultInfo.get(0).getPrincipals().get(0).getPrincipalName(), mivuser.getSystemUserName());
//    }
    
    // TODO KR-508
    /*
     * Find a System User by Principal Name
     * Test: protected List<KimEntityImpl> lookupEntitys(Map<String, String> searchCriteria) 
     */
//    public void testLookupSystemUserEntityByPrincipalName()
//    {
//        // Find the KR System User
//        Map<String,String> krSearchCriteria = new HashMap<String,String>(1);
//        krSearchCriteria.put(UCDKIMEntitySearchKeys.Person.PRINCIPAL_NAME, "kr");
//        List<EntityDefault> krSystemUserDefaultInfo = identityService.lookupEntityDefaultInfo(krSearchCriteria, true);
//        Assert.assertEquals(1, krSystemUserDefaultInfo.size());
//        Assert.assertEquals(krSystemUserDefaultInfo.get(0).getEntityId(), kruser.getSystemUserEntityId());  
//        Assert.assertEquals(krSystemUserDefaultInfo.get(0).getPrincipals().get(0).getPrincipalId(), kruser.getSystemUserId());
//        Assert.assertEquals(krSystemUserDefaultInfo.get(0).getPrincipals().get(0).getPrincipalName(), kruser.getSystemUserName());
//        // Find the KFS System User
//        Map<String,String> kfsSearchCriteria = new HashMap<String,String>(1);
//        kfsSearchCriteria.put(UCDKIMEntitySearchKeys.Person.PRINCIPAL_NAME, "kfs");
//        List<EntityDefault> kfsSystemUserDefaultInfo = identityService.lookupEntityDefaultInfo(kfsSearchCriteria, true);
//        Assert.assertEquals(1, kfsSystemUserDefaultInfo.size());
//        Assert.assertEquals(kfsSystemUserDefaultInfo.get(0).getEntityId(), kfsuser.getSystemUserEntityId());  
//        Assert.assertEquals(kfsSystemUserDefaultInfo.get(0).getPrincipals().get(0).getPrincipalId(), kfsuser.getSystemUserId());
//        Assert.assertEquals(kfsSystemUserDefaultInfo.get(0).getPrincipals().get(0).getPrincipalName(), kfsuser.getSystemUserName());         
//        // Find the Admin System User
//        Map<String,String> adminSearchCriteria = new HashMap<String,String>(1);
//        adminSearchCriteria.put(UCDKIMEntitySearchKeys.Person.PRINCIPAL_NAME, "admin");
//        List<EntityDefault> adminSystemUserDefaultInfo = identityService.lookupEntityDefaultInfo(adminSearchCriteria, true);
//        Assert.assertEquals(1, adminSystemUserDefaultInfo.size());
//        Assert.assertEquals(adminSystemUserDefaultInfo.get(0).getEntityId(), adminuser.getSystemUserEntityId());  
//        Assert.assertEquals(adminSystemUserDefaultInfo.get(0).getPrincipals().get(0).getPrincipalId(), adminuser.getSystemUserId());
//        Assert.assertEquals(adminSystemUserDefaultInfo.get(0).getPrincipals().get(0).getPrincipalName(), adminuser.getSystemUserName());    
//        // Find the MIV System User
//        Map<String,String> mivSearchCriteria = new HashMap<String,String>(1);
//        mivSearchCriteria.put(UCDKIMEntitySearchKeys.Person.PRINCIPAL_NAME, "MIV-SYSTEM");
//        List<EntityDefault> mivSystemUserDefaultInfo = identityService.lookupEntityDefaultInfo(mivSearchCriteria, true);
//        Assert.assertEquals(1, mivSystemUserDefaultInfo.size());
//        Assert.assertEquals(mivSystemUserDefaultInfo.get(0).getEntityId(), mivuser.getSystemUserEntityId());  
//        Assert.assertEquals(mivSystemUserDefaultInfo.get(0).getPrincipals().get(0).getPrincipalId(), mivuser.getSystemUserId());
//        Assert.assertEquals(mivSystemUserDefaultInfo.get(0).getPrincipals().get(0).getPrincipalName(), mivuser.getSystemUserName());
//    }
    
    // TODO MIV-3558
    public void testGetAllEntityEmailAddresses()
    {
        List<String> principalNames = new ArrayList<String>();
        principalNames.add("constance.bowe");
        principalNames.add("sgaxiola");
        principalNames.add("aljuburi");
        principalNames.add("timothy.albertson");
        principalNames.add("tdaman07");
        
        for (String principalName : principalNames)
        {
            
            Entity kimEntityInfo = identityService.getEntityByPrincipalName(principalName);
            System.out.println(kimEntityInfo.getNames().get(0).getLastName() + "," + kimEntityInfo.getNames().get(0).getFirstName());
            for (EntityTypeContactInfo kimEntityType : kimEntityInfo.getEntityTypeContactInfos())
            {
                for (EntityEmail kimEntityEmail : kimEntityType.getEmailAddresses())
                {
                    System.out.println(kimEntityEmail.getEmailAddress());
                }
            }
        }
    }   
    
    // TODO KR-541
    public void testGetDefaultAffiliation()
    {
        EntityDefault entity = identityService.getEntityDefaultByPrincipalName("katehi");
        //KimEntityInfo entity = identityService.getEntityInfoByPrincipalName("katehi");
        System.out.println("All Affiliations:");
        for (EntityAffiliation affiliation : entity.getAffiliations())
        {
            System.out.println(affiliation.getAffiliationType().getCode() + " - " + affiliation.getCampusCode());
        }
        EntityAffiliation defaultAffiliation = entity.getDefaultAffiliation();
        if (defaultAffiliation != null)
        {
            System.out.println("Default Affiliation: " + defaultAffiliation.getAffiliationType().getCode() + " - " + defaultAffiliation.getCampusCode());
        }
        else
        {
            System.out.println("No Default Affiliation");
        }
    }
}
