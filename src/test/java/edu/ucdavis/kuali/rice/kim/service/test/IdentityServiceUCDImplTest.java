/*
 * Copyright 2008-2011 The Regents of the University of California
 *
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.opensource.org/licenses/ecl2.php
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.ucdavis.kuali.rice.kim.service.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.kuali.rice.kim.api.identity.email.EntityEmail;
import org.kuali.rice.kim.api.identity.entity.Entity;
import org.kuali.rice.kim.api.identity.principal.Principal;
import org.kuali.rice.kim.api.identity.privacy.EntityPrivacyPreferences;
import org.kuali.rice.kim.api.identity.type.EntityTypeContactInfo;

//import org.kuali.rice.kim.bo.entity.KimEntityEmail;
//import org.kuali.rice.kim.bo.entity.dto.KimEntityPrivacyPreferencesInfo;
//import org.kuali.rice.kim.bo.entity.dto.KimPrincipalInfo;
//import org.kuali.rice.kim.bo.entity.impl.KimEntityEntityTypeImpl;
//import org.kuali.rice.kim.bo.entity.impl.KimEntityImpl;
import org.kuali.rice.kim.api.identity.IdentityService;
import  org.kuali.rice.krad.service.BusinessObjectService;
import org.kuali.rice.kns.service.BusinessObjectAuthorizationService;
import org.kuali.rice.kns.service.KNSServiceLocator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import edu.ucdavis.iet.commons.ldap.domain.LdapAccount;
import edu.ucdavis.iet.commons.ldap.domain.LdapPerson;
import edu.ucdavis.iet.commons.ldap.service.LdapAccountService;
import edu.ucdavis.iet.commons.ldap.service.LdapMailListingService;
import edu.ucdavis.iet.commons.ldap.service.LdapPersonService;
import edu.ucdavis.kuali.rice.kim.domain.SystemUser;
import edu.ucdavis.kuali.rice.kim.service.impl.IdentityServiceUCDImpl;
import edu.ucdavis.kuali.rice.kim.service.impl.UCDKIMEntitySearchKeys;

import junit.framework.Assert;
import junit.framework.TestCase;

public class IdentityServiceUCDImplTest extends TestCase
{
    private String[] springFiles = {"classpath:edu/ucdavis/kuali/rice/test/config/test-common.xml"
                                   ,"classpath:edu/ucdavis/kuali/rice/kim/config/ldap-data.xml"
                                   ,"classpath:edu/ucdavis/kuali/rice/test/config/whitepages-data.xml"
                                   ,"classpath:edu/ucdavis/kuali/rice/kim/config/ucd-kim-wp-service.xml"};
    
    private ApplicationContext context = new ClassPathXmlApplicationContext(springFiles);
    private IdentityService kimIdentityService = (IdentityService) context.getBean("kimIdentityDelegateService");
    private LdapAccountService ldapAccountService = (LdapAccountService) context.getBean("ldapAccountService");
    private LdapPersonService ldapPersonService = (LdapPersonService) context.getBean("ldapPersonService");
    private LdapMailListingService ldapMailListingService = (LdapMailListingService) context.getBean("ldapMailListingService");
    private LdapPerson ldapPerson = null;
    private LdapAccount ldapAccount = null;
    private final List<SystemUser> systemUserList = new ArrayList<SystemUser>();
    private final SystemUser kruser = (SystemUser)context.getBean("kruser");
    private final SystemUser kfsuser = (SystemUser)context.getBean("kfsuser");
    private final SystemUser adminuser = (SystemUser)context.getBean("adminuser");
    private final SystemUser mivuser = (SystemUser)context.getBean("mivuser");

    private BusinessObjectAuthorizationService businessObjectService;
    
    protected BusinessObjectAuthorizationService getBusinessObjectService() {
        if ( businessObjectService == null ) {
                businessObjectService = KNSServiceLocator.getBusinessObjectAuthorizationService();
        }
        return businessObjectService;
}
    
    protected void setUp() throws Exception
    {
        super.setUp();
        // Find "model citizen" by email address.
        String ucdPersonUUID1 = ldapMailListingService.findByEmailAddress("chancellor@ucdavis.edu").get(0).getUcdPersonUUID();
        ldapPerson = ldapPersonService.findByUniversalUserId(ucdPersonUUID1);
        ldapAccount = ldapAccountService.findByUserId(ldapPerson.getUid().get(0), "IKRB");
        // Populate the System User list
        systemUserList.add(kruser);
        systemUserList.add(kfsuser);
        systemUserList.add(adminuser);
        systemUserList.add(mivuser);
        
    }
    
    // TODO KR-508
    /*
     * Find a System User by Entity ID 
     * Test: public KimEntityImpl getEntityImpl(String entityId)
     */
    public void testGetSystemUserEntityImpl()
    {
        IdentityServiceUCDImpl identityServiceUcdImpl = new IdentityServiceUCDImpl();
        identityServiceUcdImpl.setLdapAccountService(ldapAccountService);
        identityServiceUcdImpl.setLdapMailListingService(ldapMailListingService);
        identityServiceUcdImpl.setLdapPersonService(ldapPersonService);
        identityServiceUcdImpl.setSystemUserList(systemUserList);
        // Find the KR System User
        Entity krSystemUser = identityServiceUcdImpl.getEntity("1");
        Assert.assertEquals(krSystemUser.getId(), "1");  
        Assert.assertEquals(krSystemUser.getPrincipals().get(0).getEntityId(), "1");
        // Find the KFS System User
        Entity kfsSystemUser = identityServiceUcdImpl.getEntity("2");
        Assert.assertEquals(kfsSystemUser.getId(), "2");  
        Assert.assertEquals(kfsSystemUser.getPrincipals().get(0).getEntityId(), "2");
        // Find the Admin System User
        Entity adminSystemUser = identityServiceUcdImpl.getEntity("1100");
        Assert.assertEquals(adminSystemUser.getId(), "1100");  
        Assert.assertEquals(adminSystemUser.getPrincipals().get(0).getEntityId(), "1100");
        // Find the MIV System User
        Entity mivSystemUser = identityServiceUcdImpl.getEntity("9001");
        Assert.assertEquals(mivSystemUser.getId(), "9001");  
        Assert.assertEquals(mivSystemUser.getPrincipals().get(0).getEntityId(), "9001");
    }
    
    // TODO KR-508
    /*
     * Find a System User by Principal ID
     * Test: public KimPrincipalImpl getPrincipalImpl(String principalId) 
     */
    public void testGetSystemUserPrincipalImpl()
    {
        // Find the KR System User
        Principal krSystemUser = kimIdentityService.getPrincipal(kruser.getSystemUserId());
        Assert.assertEquals(krSystemUser.getEntityId(), "1");
        Assert.assertEquals(krSystemUser.getPrincipalId(), "1");
        Assert.assertEquals(krSystemUser.getPrincipalName(), "kr");
        // Find the KFS System User
        Principal kfsSystemUser = kimIdentityService.getPrincipal(kfsuser.getSystemUserId());
        Assert.assertEquals(kfsSystemUser.getEntityId(), "2");
        Assert.assertEquals(kfsSystemUser.getPrincipalId(), "2");
        Assert.assertEquals(kfsSystemUser.getPrincipalName(), "kfs");
        // Find the Admin System User
        Principal adminSystemUser = kimIdentityService.getPrincipal(adminuser.getSystemUserId());
        Assert.assertEquals(adminSystemUser.getEntityId(), "1100");
        Assert.assertEquals(adminSystemUser.getPrincipalId(), "1100");
        Assert.assertEquals(adminSystemUser.getPrincipalName(), "admin");
        // Find the MIV System User
        Principal mivSystemUser = kimIdentityService.getPrincipal(mivuser.getSystemUserId());
        Assert.assertEquals(mivSystemUser.getEntityId(), "9001");
        Assert.assertEquals(mivSystemUser.getPrincipalId(), "9001");
        Assert.assertEquals(mivSystemUser.getPrincipalName(), "MIV-SYSTEM");
    }
       
    // TODO KR-508
    /*
     * Find a System User by Principal Name
     * Test: public KimPrincipalInfo getPrincipalByPrincipalName(String principalName)
     */
    public void testGetSystemUserPrincipalByPrincipalName()
    {
        // Find the KR System User
    	Principal krSystemUser = kimIdentityService.getPrincipalByPrincipalName(kruser.getSystemUserName());
        Assert.assertEquals(krSystemUser.getEntityId(), "1");
        Assert.assertEquals(krSystemUser.getPrincipalId(), "1");
        Assert.assertEquals(krSystemUser.getPrincipalName(), "kr");
        // Find the KFS System User
        Principal kfsSystemUser = kimIdentityService.getPrincipalByPrincipalName(kfsuser.getSystemUserName());
        Assert.assertEquals(kfsSystemUser.getEntityId(), "2");
        Assert.assertEquals(kfsSystemUser.getPrincipalId(), "2");
        Assert.assertEquals(kfsSystemUser.getPrincipalName(), "kfs");
        // Find the Admin System User
        Principal adminSystemUser = kimIdentityService.getPrincipalByPrincipalName(adminuser.getSystemUserName());
        Assert.assertEquals(adminSystemUser.getEntityId(), "1100");
        Assert.assertEquals(adminSystemUser.getPrincipalId(), "1100");
        Assert.assertEquals(adminSystemUser.getPrincipalName(), "admin");
        // Find the MIV System User
        Principal mivSystemUser = kimIdentityService.getPrincipalByPrincipalName(mivuser.getSystemUserName());
        Assert.assertEquals(mivSystemUser.getEntityId(), "9001");
        Assert.assertEquals(mivSystemUser.getPrincipalId(), "9001");
        Assert.assertEquals(mivSystemUser.getPrincipalName(), "MIV-SYSTEM");
    }
    // TODO KR-532
    public void testGetEntityImplByBlankEntityId()
    {
        Entity kimEntityImpl = null;
        IdentityServiceUCDImpl identityServiceUcdImpl = new IdentityServiceUCDImpl();
        identityServiceUcdImpl.setLdapAccountService(ldapAccountService);
        identityServiceUcdImpl.setLdapMailListingService(ldapMailListingService);
        identityServiceUcdImpl.setLdapPersonService(ldapPersonService);
        identityServiceUcdImpl.setSystemUserList(systemUserList);
        kimEntityImpl = identityServiceUcdImpl.getEntity("");
        Assert.assertNull(kimEntityImpl);
    }
    
    /*
     * Find an Entity by Entity ID
     */
    public void testGetEntityImplByEntityId()
    {

        Entity kimEntityImpl = null;
        IdentityServiceUCDImpl identityServiceUcdImpl = new IdentityServiceUCDImpl();
        identityServiceUcdImpl.setLdapAccountService(ldapAccountService);
        identityServiceUcdImpl.setLdapMailListingService(ldapMailListingService);
        identityServiceUcdImpl.setLdapPersonService(ldapPersonService);
        identityServiceUcdImpl.setSystemUserList(systemUserList);
        kimEntityImpl = identityServiceUcdImpl.getEntity(ldapPerson.getUcdPersonUUID());
        Assert.assertNotNull(kimEntityImpl);
        Assert.assertEquals(kimEntityImpl.getId(), ldapPerson.getUcdPersonUUID());   
    }
    
    /*
     * Find a Principal by a valid Principal ID
     */
    public void testGetPrincipalImplByValidPrincipalId()
    {   
        String principalId = ldapAccount.getUidNumber();
        Principal kimPrincipalInfo = null;
        kimPrincipalInfo = kimIdentityService.getPrincipal(principalId);
        Assert.assertNotNull(kimPrincipalInfo);
        Assert.assertEquals(kimPrincipalInfo.getEntityId(), ldapPerson.getUcdPersonUUID());
    }
    
    /*
     * Find a Principal by an invalid Principal ID
     */
    public void testGetPrincipalImplByInvalidPrincipalId()
    {   
        String principalId = "Some invalid Principal ID";
        Principal kimPrincipalInfo = kimIdentityService.getPrincipal(principalId);
        Assert.assertNull(kimPrincipalInfo);
    }

    /*
     * Find a Principal by a valid Principal Name
     */
    public void testGetPrincipalByValidPrincipalName()
    {
        Principal kimPrincipalInfo = null;
        String principalName = ldapAccount.getUid();
        kimPrincipalInfo = kimIdentityService.getPrincipalByPrincipalName(principalName);        
        Assert.assertNotNull(kimPrincipalInfo);
        Assert.assertEquals(kimPrincipalInfo.getEntityId(), ldapPerson.getUcdPersonUUID());
        Assert.assertTrue(kimPrincipalInfo.isActive());
    }
    
    /*
     * Find a Principal by an invalid Principal Name
     */
    public void testGetPrincipalByInvalidPrincipalName()
    {
        String principalName = "Some invalid Principal Name";
        Principal kimPrincipalInfo = kimIdentityService.getPrincipalByPrincipalName(principalName);        
        Assert.assertNull(kimPrincipalInfo);
    }

    /*
     * Find an e-mail address that is the KIM Entity's default e-mail address
     */
    public void testFindDefaultEmail()
    {
        // Find the KIM Entity
        IdentityServiceUCDImpl identityServiceUcdImpl = new IdentityServiceUCDImpl();
        identityServiceUcdImpl.setLdapAccountService(ldapAccountService);
        identityServiceUcdImpl.setLdapMailListingService(ldapMailListingService);
        identityServiceUcdImpl.setLdapPersonService(ldapPersonService);
        identityServiceUcdImpl.setSystemUserList(systemUserList);
        Entity kimEntityImpl = identityServiceUcdImpl.getEntity(ldapPerson.getUcdPersonUUID());

        boolean entityDefaultEmailFound = false;
        for (EntityTypeContactInfo kimEntityType : kimEntityImpl.getEntityTypeContactInfos())
        {
            
            for (EntityEmail kimEntityEmail : kimEntityType.getEmailAddresses())
            {
                if (kimEntityEmail.isActive() && kimEntityEmail.isDefaultValue() && kimEntityEmail.getEmailType().equals("PRIMARY"))
                {
                    entityDefaultEmailFound = true;
                }
            }
        }
        Assert.assertTrue(entityDefaultEmailFound);
    }

//    /*
//     * Find matching Entity count using sound criteria
//     */
//    public void testGetMatchingEntityCountValidCriteria()
//    {
//        Map<String, String> soundCriteria = new HashMap<String, String>(3);
//        soundCriteria.put(UCDKIMEntitySearchKeys.Person.PRINCIPAL_NAME, ldapPerson.getUid().get(0));
//        soundCriteria.put(UCDKIMEntitySearchKeys.Person.LAST_NAME, ldapPerson.getSn().get(0));
//        Assert.assertEquals(kimIdentityService.getMatchingEntityCount(soundCriteria), 1);
//    }

//    /*
//     * Find matching Entity count using runaway criteria
//     */
//    public void testGetMatchingEntityCountRunawayCriteria()
//    {
//        Map<String, String> runawayCriteria = new HashMap<String, String>(3);
//        runawayCriteria.put(UCDKIMEntitySearchKeys.Person.CITY_NAME, "efrewret");
//        runawayCriteria.put(UCDKIMEntitySearchKeys.Person.PRINCIPAL_NAME, "345234624yhdgbh");
//        runawayCriteria.put(UCDKIMEntitySearchKeys.Person.LAST_NAME, "dfhdfhdghjkrtk");
//        Assert.assertEquals(kimIdentityService.getMatchingEntityCount(runawayCriteria), 0);
//    }
 
    /*
     * All privacy settings should always be false.
     */
    public void testGetEntityPrivacyPreferences()
    {

        EntityPrivacyPreferences kimEntityPrivacyPreferencesInfo = kimIdentityService.getEntityPrivacyPreferences("someEntityID");
        Assert.assertFalse(kimEntityPrivacyPreferencesInfo.isSuppressAddress());
        Assert.assertFalse(kimEntityPrivacyPreferencesInfo.isSuppressEmail());
        Assert.assertFalse(kimEntityPrivacyPreferencesInfo.isSuppressName());
        Assert.assertFalse(kimEntityPrivacyPreferencesInfo.isSuppressPersonal());
        Assert.assertFalse(kimEntityPrivacyPreferencesInfo.isSuppressPhone());
    }

    /*
     * Finding Principal by Principal Name and password should return IllegalArgumentException all the time.
     */
    public void testGetPrincipalByPrincipalNameAndPassword()
    {

        IllegalArgumentException iae = null;
        try
        {
            kimIdentityService.getPrincipalByPrincipalNameAndPassword("somePrincipalName", "somePassword");
        }
        catch (IllegalArgumentException e)
        {
            iae = e;
        }
        Assert.assertNotNull(iae);
    }    
    
}