/*
 * Copyright 2008-2011 The Regents of the University of California
 *
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.opensource.org/licenses/ecl2.php
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.ucdavis.kuali.rice.kim.service.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.kuali.rice.kim.api.identity.entity.EntityDefault;
import org.kuali.rice.kim.api.identity.entity.Entity;
import org.kuali.rice.kim.api.identity.principal.Principal;

import edu.ucdavis.iet.commons.ldap.domain.LdapAccount;
import edu.ucdavis.iet.commons.ldap.domain.LdapMailListing;
import edu.ucdavis.iet.commons.ldap.domain.LdapPerson;
import edu.ucdavis.iet.commons.ldap.domain.impl.LdapAccountImpl;
import edu.ucdavis.iet.commons.ldap.domain.impl.LdapPersonImpl;
import edu.ucdavis.iet.commons.ldap.domain.util.LdapAccountConstants;
import edu.ucdavis.iet.commons.ldap.service.impl.LdapAccountServiceImpl;
import edu.ucdavis.iet.commons.ldap.service.impl.LdapMailListingServiceImpl;
import edu.ucdavis.iet.commons.ldap.service.impl.LdapPersonServiceImpl;
import edu.ucdavis.kuali.rice.kim.domain.SystemUser;
import edu.ucdavis.kuali.rice.kim.service.impl.IdentityServiceUCDImpl;

import junit.framework.Assert;
import junit.framework.TestCase;

public class KR610Test extends TestCase {
   
	/*
	 * The following is a collection of data that models a person who:
	 * 1) Can be found via the Person Service by e-mail address.
	 * 2) Can be found via the Account Service by a match UID number.
	 * 3) Cannot be found via the Listing Service.
	 */
	String someUcdPersonUUID = "UUID: 1";
	String someUidNumber = "UID Number: 1234";
	String someUid = "UID Name: Kerberos Principal";
	String[] uidArray = new String[] {someUid};
	String someEmail = "lonee-mail@ucdavis.edu";
	String[] emailArray = new String[] {someEmail};
	String someServiceName = "someServiceName";

    protected void setUp() throws Exception
    {
        super.setUp();    
    }
    
    /*
     * This unit test tests whether KIM will return Entity and Principal information for a 
     * person who has no e-mails that can be fetched by the LDAP Mail Listing service.
     * See KR-610.
     */
    public void testKR610() {
    	List<SystemUser> systemUserList = new ArrayList<SystemUser>();
    	SystemUser systemUser = new SystemUser();
    	systemUser.setSystemUserEntityId("S1");
    	systemUser.setSystemUserId("SU1");
    	systemUser.setSystemUserName("systemuserName");
    	systemUserList.add(systemUser);
    	
    	MockIdentityServiceUcdImpl kimIdentityDelegateService = new MockIdentityServiceUcdImpl();
    	kimIdentityDelegateService.setLdapAccountService(new MockLdapAccountService());
    	kimIdentityDelegateService.setLdapMailListingService(new MockLdapListingService());
    	kimIdentityDelegateService.setLdapPersonService(new MockLdapPersonService());
    	kimIdentityDelegateService.setSystemUserList(systemUserList);
    	
    	EntityDefault edi = kimIdentityDelegateService.getEntityDefault(someUcdPersonUUID);
    	Assert.assertEquals(edi.getEntityId(), someUcdPersonUUID);
    	Assert.assertTrue(!edi.getPrincipals().isEmpty());
    	System.out.println(edi.getPrincipals().get(0).getEntityId());
    	System.out.println(edi.getPrincipals().get(0).getPrincipalId());
    	System.out.println(edi.getPrincipals().get(0).getPrincipalName());
    }
    private class MockLdapPersonService extends LdapPersonServiceImpl {
		
        @Override
        public LdapPerson findByUniversalUserId(String uuid)
        {
        	LdapPersonImpl ldapPerson = new LdapPersonImpl();
			ldapPerson.setUcdPersonUUID(someUcdPersonUUID);
			ldapPerson.setUid(uidArray);
			ldapPerson.setMail(emailArray);
            return ldapPerson;
        }
        
    	@Override
	    public List<LdapPerson> findByCriteria(Map<String, String> searchCriteria, boolean exactMatch) {
			LdapPersonImpl ldapPerson = new LdapPersonImpl();
			ldapPerson.setUcdPersonUUID(someUcdPersonUUID);
			ldapPerson.setUid(uidArray);
			ldapPerson.setMail(emailArray);
			List<LdapPerson> ldapPersonList = new ArrayList<LdapPerson>();
			ldapPersonList.add(ldapPerson);
			return ldapPersonList;
		}
    }
	
    private class MockLdapListingService extends LdapMailListingServiceImpl {
    	
    	@Override
        public List<LdapMailListing> findByEmailAddress(String mail) {
    		return null;
    	}
    	
    	@Override
        public LdapMailListing findPrimaryMailListing(String uuid)
        {
            return null;
        }
    	
    	@Override
        public List<LdapMailListing> findMailListings(String uuid)
        {
            return null;
        }
    }
    
    private class MockLdapAccountService extends LdapAccountServiceImpl {
    	
    	@Override
        public LdapAccount findByUserId(String uid, String serviceName) {
    		Map<String, String> ldapAccountContextMap = new HashMap<String, String>();
    		ldapAccountContextMap.put(LdapAccountConstants.USER_ID_NUMBER, someUidNumber);
    		ldapAccountContextMap.put(LdapAccountConstants.USER_ID, someUid);
    		
    		LdapAccountImpl ldapAccount = new LdapAccountImpl(ldapAccountContextMap);
			return ldapAccount;
    	}
    }
    
    private class MockIdentityServiceUcdImpl extends IdentityServiceUCDImpl {
    	
//    	@Override
    	protected EntityDefault convertEntityImplToDefaultInfo( Entity entity ) {
    		EntityDefault.Builder info = EntityDefault.Builder.create(entity.getId());
    		info.setEntityId( entity.getId() );
    		info.setActive( entity.isActive() );
    		ArrayList<Principal.Builder> principalInfo = new ArrayList<Principal.Builder>( entity.getPrincipals().size() );
    		info.setPrincipals( principalInfo );
    		for ( Principal p : entity.getPrincipals() ) {
    			principalInfo.add( Principal.Builder.create( p ) );
    		}
    		return info.build();
    	}
    	
    }
    public void testReturnPersonListing()
    {
    	MockLdapPersonService ldapPersonService = new MockLdapPersonService();
    	Map<String, String> searchCriteria = new HashMap<String,String>();
    	searchCriteria.put("someKey", "someValue");
        List<LdapPerson> ldapPerson = ldapPersonService.findByCriteria(searchCriteria);
    	Assert.assertEquals(ldapPerson.get(0).getUcdPersonUUID(), someUcdPersonUUID);
    	Assert.assertEquals(ldapPerson.get(0).getUid().get(0), someUid);
    	Assert.assertEquals(ldapPerson.get(0).getMail().get(0), someEmail);    	
    }
    
    public void testReturnAccountListing()
    {
    	MockLdapAccountService ldapAccountService = new MockLdapAccountService();
    	LdapAccount ldapAccount = ldapAccountService.findByUserId(someUid, someServiceName);
    	Assert.assertEquals(ldapAccount.getUid(), someUid);	
    	Assert.assertEquals(ldapAccount.getUidNumber(), someUidNumber);
    }
    
    public void testReturnNullMailListing()
    {
        MockLdapListingService ldapListingService = new MockLdapListingService();
        List<LdapMailListing> ldapMailListing = ldapListingService.findByEmailAddress("somee-mail@ucdavis.edu");
    	Assert.assertNull(ldapMailListing);
    }
 }